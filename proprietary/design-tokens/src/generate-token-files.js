/* eslint-disable */

const fs = require('fs');

const kebabCase = (str) => {
  return str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    ?.join('-')
    ?.toLowerCase();
};

function generateTokenFiles() {
  const tokenData = fs.readFileSync('./src/figma-tokens.json', 'utf8');
  const data = JSON.parse(tokenData);

  const tokenSetOrder = data.$metadata.tokenSetOrder;
  if (Array.isArray(tokenSetOrder)) {
    tokenSetOrder.forEach((tokenset) => {
      const theme = {};
      Object.keys(data[tokenset]).forEach((key) => {
        const value = data[tokenset][key];
        theme[key] = value;
      });
      const tokenJSON = JSON.stringify(theme, null, 2);
      fs.promises.mkdir('src/default', { recursive: true }).catch(console.error);
      fs.writeFileSync(`src/default/${kebabCase(tokenset)}.tokens.json`, tokenJSON + '\n', {});
    });
  }
}
generateTokenFiles();
