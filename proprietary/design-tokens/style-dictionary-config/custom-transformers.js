module.exports = {
  name: 'shadow/spreadShadow',
  type: 'value',
  transitive: true,
  matcher: (prop) => prop.original.value.type === 'dropShadow',
  transformer: function (prop) {
    // destructure shadow values from original token value
    const { x, y, blur, spread, color } = prop.original.value;

    return `${x}px ${y}px ${blur}px ${spread}px ${color}`;
  },
};
