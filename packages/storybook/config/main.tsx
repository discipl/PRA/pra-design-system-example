import { dirname, join } from 'path';
/* eslint-env node */
module.exports = {
  stories: [
    '../src/**/*doc.@(js|jsx|mdx|ts|tsx)',
    '../../components-react/src/components/**/*stories.@(js|jsx|mdx|ts|tsx)',
  ],
  addons: [
    getAbsolutePath('@etchteam/storybook-addon-status'),
    getAbsolutePath('@storybook/addon-a11y'),
    getAbsolutePath('@storybook/addon-docs'),
    getAbsolutePath('@storybook/addon-viewport'),
    getAbsolutePath('@storybook/blocks'),
    getAbsolutePath('@storybook/preset-scss'),
    getAbsolutePath('@storybook/addon-mdx-gfm'),
    getAbsolutePath('@storybook/addon-mdx-gfm'),
  ],
  framework: {
    name: getAbsolutePath('@storybook/react-vite'),
    options: {},
  },
  core: {
    disableTelemetry: true,
  },
  docs: {
    autodocs: true,
  },
  features: {
    buildStoriesJson: true,
  },
  staticDirs: ['../../../proprietary/assets/src'],
  typescript: {
    reactDocgen: 'react-docgen-typescript',
  },
};

function getAbsolutePath(value: string): string {
  return dirname(require.resolve(join(value, 'package.json')));
}
