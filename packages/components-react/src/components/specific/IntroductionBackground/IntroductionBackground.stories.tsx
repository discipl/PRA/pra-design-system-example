import ReadMe from '@persoonlijke-regelingen-assistent/components-css/IntroductionBackground/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { IntroductionBackground } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/alternate/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof IntroductionBackground> = {
  component: IntroductionBackground,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/SpecificComponents/IntroductionBackground',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'introductionBackground',
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
  },
};

export default meta;
type Story = StoryObj<typeof IntroductionBackground>;

export const Default: Story = {};
// export const AlternateTheme: Story = {
//   decorators: [
//     (Story) => (
//       <div
//         className="pra-alternate-theme"
//         style={{
//           width: '100%',
//           height: '100%',
//         }}
//       >
//         <Story />
//       </div>
//     ),
//   ],
// };
