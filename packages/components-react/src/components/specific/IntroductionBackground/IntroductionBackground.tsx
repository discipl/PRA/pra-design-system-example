import BezwaarEnBeroep from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/BezwaarEnBeroep';
import Idkaart from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Idkaart';
import KopenEnVerkopen from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/KopenEnVerkopen';
import Nieuwsbrief from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Nieuwsbrief';
import Prijskaartje from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Prijskaartje';
import Vacatures from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Vacatures';
import Vrijwilligerswerk from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Vrijwilligerswerk';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithRef } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/IntroductionBackground/IntroductionBackground.scss';

interface IntroductionBackgroundSpecificProps {}

export interface IntroductionBackgroundProps
  extends HTMLAttributes<HTMLDivElement>,
    IntroductionBackgroundSpecificProps {}

export const IntroductionBackground: React.ForwardRefExoticComponent<
  IntroductionBackgroundProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ className }: PropsWithRef<IntroductionBackgroundProps>, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <div ref={ref} className={`pra-background ${className ? className : ''}`}>
      {Nieuwsbrief({
        style: { width: '20%', position: 'absolute', right: '40%', top: '0%', transform: 'rotate(15deg)' },
      })}
      {Vacatures({
        style: { width: '20%', position: 'absolute', right: '10%', top: '15%', transform: 'rotate(10deg)' },
      })}
      {Idkaart({
        style: { width: '25%', position: 'absolute', left: '5%', top: '23%', transform: 'rotate(-2deg)' },
      })}
      {BezwaarEnBeroep({ style: { width: '25%', position: 'absolute', right: '-5%', top: '31%' } })}
      {Prijskaartje({
        style: { width: '25%', position: 'absolute', left: '-10%', top: '55%', transform: 'rotate(15deg)' },
      })}
      {KopenEnVerkopen({
        style: { width: '20%', position: 'absolute', right: '-5%', top: '55%', transform: 'rotate(10deg)' },
      })}
      {Vrijwilligerswerk({
        style: { width: '18%', position: 'absolute', left: '40%', bottom: '25%', transform: 'rotate(-20deg)' },
      })}
      {BezwaarEnBeroep({ style: { width: '25%', position: 'absolute', left: '8%', bottom: '2%' } })}
      {Nieuwsbrief({
        style: { width: '20%', position: 'absolute', right: '7%', bottom: '2%', transform: 'rotate(10deg)' },
      })}
    </div>
  );
});

IntroductionBackground.displayName = 'IntroductionBackground';
