import Shape from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/Shape';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import Settings from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Settings';
import { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode } from 'react';
import { NavigationBar } from '../../generic/NavigationBar';
import { NavigationBarItem } from '../../generic/NavigationBarItem';

interface PraNavigationBarSpecificProps {
  selectedItem?: '' | 'Dashboard' | 'Profiel' | 'Instellingen';
  indicators?: ('Dashboard' | 'Profiel' | 'Instellingen')[];
  profileOnClickHandler: MouseEventHandler;
  dashboardOnClickHandler: MouseEventHandler;
  settingsOnClickHandler: MouseEventHandler;
  profileIcon?: ReactNode;
}
export interface PraNavigationBarProps extends HTMLAttributes<HTMLDivElement>, PraNavigationBarSpecificProps {}

export const PraNavigationBar: React.ForwardRefExoticComponent<
  PraNavigationBarProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      profileOnClickHandler,
      dashboardOnClickHandler,
      settingsOnClickHandler,
      selectedItem = '',
      indicators = [],
      profileIcon,
      ...props
    }: PraNavigationBarProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <NavigationBar {...props} ref={ref}>
        <NavigationBarItem
          selected={selectedItem === 'Dashboard'}
          indicator={indicators.includes('Dashboard')}
          text="Dashboard"
          icon={LayoutGrid({})}
          onClickHandler={dashboardOnClickHandler}
        />
        <NavigationBarItem
          selected={selectedItem === 'Profiel'}
          indicator={indicators.includes('Profiel')}
          text="Profiel"
          icon={profileIcon ? profileIcon : Shape({})}
          onClickHandler={profileOnClickHandler}
        />
        <NavigationBarItem
          selected={selectedItem === 'Instellingen'}
          indicator={indicators.includes('Instellingen')}
          text="Instellingen"
          icon={Settings({})}
          onClickHandler={settingsOnClickHandler}
        />
      </NavigationBar>
    );
  },
);

PraNavigationBar.displayName = 'PraNavigationBar';
