import type { Meta, StoryObj } from '@storybook/react';
import { PraNavigationBar } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof PraNavigationBar> = {
  component: PraNavigationBar,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/SpecificComponents/PraNavigationBar',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof PraNavigationBar>;

export const Default: Story = {
  args: {
    dashboardOnClickHandler: () => {},
    settingsOnClickHandler: () => {},
    profileOnClickHandler: () => {},
    selectedItem: 'Instellingen',
    indicators: ['Instellingen', 'Profiel'],
  },
};
