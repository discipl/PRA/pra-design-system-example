import webDeveloperPng from '@persoonlijke-regelingen-assistent/assets/dist/images/web-developer.png';
import React, { ForwardedRef, forwardRef, PropsWithChildren, useEffect, useState } from 'react';
import { Image } from '../../generic/Image';
import { Paragraph } from '../../generic/Paragraph';
import '@persoonlijke-regelingen-assistent/components-css/Spinner/Spinner.scss';

interface SpinnerSpecificProps {}

export interface SpinnerProps extends SpinnerSpecificProps {}

export const Spinner: React.ForwardRefExoticComponent<
  SpinnerProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ ...otherProps }: PropsWithChildren<SpinnerProps>, ref: ForwardedRef<HTMLDivElement>) => {
  const [dots, setDots] = useState(0);

  useEffect(() => {
    const id = setInterval(
      () =>
        setDots((oldValue) => {
          if (oldValue === 3) {
            return 0;
          } else {
            return oldValue + 1;
          }
        }),
      900,
    );

    return () => {
      clearInterval(id);
    };
  }, []);

  return (
    <div ref={ref} className="pra-spinner" {...otherProps}>
      <Image className="utrecht-img--scale-down" src={webDeveloperPng} alt={'spinner'} />
      <Paragraph className="pra-spinner-header">{`Aan het laden`}</Paragraph>
      <Paragraph className="pra-spinner-dots-label">{`Wacht even${'.'.repeat(dots)}`}</Paragraph>
    </div>
  );
});

Spinner.displayName = 'Spinner';
