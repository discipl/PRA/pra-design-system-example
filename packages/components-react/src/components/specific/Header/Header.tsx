import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, ReactElement } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Header/Header.scss';

interface HeaderBarTitleSpecificProps {
  leftNode?: ReactElement | null;
  rightNode?: ReactElement | null;
}
export interface HeaderBarTitleProps extends HTMLAttributes<HTMLDivElement>, HeaderBarTitleSpecificProps {}

export const Header: React.ForwardRefExoticComponent<
  HeaderBarTitleProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ leftNode, rightNode }: PropsWithChildren<HeaderBarTitleProps>, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <div ref={ref} className="pra-header">
      {leftNode && <span className="pra-header-node-left">{leftNode}</span>}
      {rightNode && <span className="pra-header-node-right">{rightNode}</span>}
    </div>
  );
});

Header.displayName = 'Header';
