import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import Politiek from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Politiek';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Header/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { CSSProperties } from 'react';
import { Header } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { Icon } from '../../generic/Icon';

const meta: Meta<typeof Header> = {
  component: Header,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/SpecificComponents/Header',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'header',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Header>;

export const Default: Story = {
  args: {
    leftNode: (
      <BreadcrumbNav>
        <BreadcrumbNavLink
          style={{ '--utrecht-icon-inset-block-start': '-0.1em' } as CSSProperties}
          current={false}
          href={``}
          index={0}
        >
          <Icon>{LayoutGrid({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavLink>
        <BreadcrumbNavSeparator>
          <Icon>{ChevronRight({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavSeparator>

        <BreadcrumbNavLink current={false} href={``} index={1}>
          Thema&apos;s
        </BreadcrumbNavLink>
      </BreadcrumbNav>
    ),
  },
};

export const WithTwoNodes: Story = {
  args: {
    leftNode: (
      <BreadcrumbNav>
        <BreadcrumbNavLink
          style={{ '--utrecht-icon-inset-block-start': '-0.1em' } as CSSProperties}
          current={false}
          href={``}
          index={0}
        >
          <Icon>{LayoutGrid({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavLink>
        <BreadcrumbNavSeparator>
          <Icon>{ChevronRight({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavSeparator>

        <BreadcrumbNavLink current={false} href={``} index={1}>
          Thema&apos;s
        </BreadcrumbNavLink>
      </BreadcrumbNav>
    ),
    rightNode: <>{Politiek({})}</>,
  },
};
