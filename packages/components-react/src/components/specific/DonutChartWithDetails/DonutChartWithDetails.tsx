import '@persoonlijke-regelingen-assistent/components-css/DonutChartWithDetails/DonutChartWithDetails.scss';
import SvgDownload from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Download';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, ReactNode } from 'react';
import { Button } from '../../generic/Button';
import { DonutChart } from '../../generic/DonutChart';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { ListCalculation } from '../../generic/ListCalculation';
import { Tabs } from '../../generic/Tabs';

interface Amount {
  Label: string;
  AmountType: 'Total' | 'Single' | 'Unknown';
  Color?: string;
  Amount?: number;
  Parts?: {
    Values: number[];
    Labels: string[];
  };
}
interface Calculation {
  Label: string;
  CalculationType: 'Summation' | 'Substraction';
  Amounts?: Amount[];
  TotalAmount?: Amount;
  ResultingAmount?: Amount;
}
interface DonutChartWithDetailsSpecificProps {
  HeaderAmount?: { Label: string; Color: string; AmountType: string; Amount: number };
  Calculations: Calculation[];
}

export interface DonutChartWithDetailsProps
  extends HTMLAttributes<HTMLDivElement>,
    DonutChartWithDetailsSpecificProps {}

export const DonutChartWithDetails: React.ForwardRefExoticComponent<
  DonutChartWithDetailsProps & {
    children?: ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { HeaderAmount, Calculations }: PropsWithChildren<DonutChartWithDetailsProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className="pra-donutchartwithdetails">
        <Heading level={3}>{HeaderAmount?.Label}</Heading>
        <Heading level={3} style={{ color: HeaderAmount?.Color }}>
          {HeaderAmount?.Amount}
        </Heading>
        <Tabs
          tabData={[
            {
              label: 'Diagrammen',
              panelContent: (
                <GridLayout templateColumns={6}>
                  {Calculations.map((calculation: Calculation) => {
                    const { Amounts = [], ResultingAmount } = calculation;
                    const donutChartData = [ResultingAmount, ...Amounts].reduce(
                      (
                        donutChartData: { key: string; value: number; stroke: string; fill: string }[],
                        amount: Amount | undefined,
                      ) => {
                        if (amount && amount.AmountType !== 'Unknown') {
                          donutChartData.push({
                            key: `${amount.Label}`,
                            value: amount?.Amount || 0,
                            stroke: amount?.Color || '#AAAAAA',
                            fill: amount?.Color || '#AAAAAA',
                          });
                        }
                        return donutChartData;
                      },
                      [],
                    );
                    return [
                      <GridLayoutCell columnSpan={6} alignItems="start" paddingInline={0}>
                        <Heading level={3}>{calculation.Label}</Heading>
                      </GridLayoutCell>,
                      <GridLayoutCell columnSpan={6} alignItems="center" paddingInline={0}>
                        <DonutChart
                          radius={200}
                          arcWidth={45}
                          data={donutChartData}
                          showLabels={false}
                          donutValue={`€ ${calculation.TotalAmount?.Amount}`}
                        />
                      </GridLayoutCell>,
                    ];
                  })}
                </GridLayout>
              ),
            },
            {
              label: 'Opbouw Berekening',
              panelContent: (
                <GridLayout templateColumns={6}>
                  {Calculations.map((calculation: Calculation, calculationIndex) => {
                    const { Amounts = [], ResultingAmount, TotalAmount } = calculation;

                    const ListCalculationTotalAmount = TotalAmount
                      ? {
                          amount: TotalAmount?.Amount,
                          label: TotalAmount?.Label || '',
                        }
                      : undefined;

                    const ListCalculationResultingAmount = ResultingAmount
                      ? {
                          amount: ResultingAmount?.Amount,
                          label: TotalAmount?.Label || '',
                        }
                      : undefined;

                    const ListCalculationAmounts = Amounts.map((amount: Amount) => amount).reduce(
                      (
                        ListCalculationAmounts: {
                          label: string;
                          amount?: number;
                          isSubpart?: boolean;
                        }[],
                        amount: Amount | undefined,
                      ) => {
                        if (amount) {
                          ListCalculationAmounts.push({ amount: amount?.Amount, label: amount?.Label || '' });
                          if (amount?.AmountType === 'Total') {
                            amount.Parts?.Labels?.forEach((label, index) => {
                              ListCalculationAmounts.push({
                                amount: amount.Parts?.Values?.[index],
                                label,
                                isSubpart: true,
                              });
                            });
                          }
                        }
                        return ListCalculationAmounts;
                      },
                      [],
                    );

                    //calculations.CalculationType === 'Summation'
                    let listCalculationData = [
                      ...ListCalculationAmounts,
                      ...(ListCalculationResultingAmount ? [ListCalculationResultingAmount] : []),
                      ...(ListCalculationTotalAmount ? [{ ...ListCalculationTotalAmount }] : []),
                    ];
                    if (calculation.CalculationType === 'Substraction') {
                      listCalculationData = [
                        ...(ListCalculationTotalAmount ? [ListCalculationTotalAmount] : []),
                        ...ListCalculationAmounts,
                        ...(ListCalculationResultingAmount ? [{ ...ListCalculationResultingAmount }] : []),
                      ];
                    }

                    return (
                      <GridLayoutCell columnSpan={6} alignItems="center" paddingInline={0}>
                        <ListCalculation
                          title={calculation.Label}
                          key={`${calculationIndex}`}
                          initCollapsed={false}
                          data={listCalculationData}
                          calculationSymbol={calculation.CalculationType === 'Substraction' ? '-' : '+'}
                        />
                      </GridLayoutCell>
                    );
                  })}
                </GridLayout>
              ),
            },
          ]}
        />
        <Button appearance="subtle-button" onClick={() => {}}>
          <Icon>{SvgDownload({ fill: 'var(--pra-color-secondary-600)' })}</Icon> Download
        </Button>
      </div>
    );
  },
);

DonutChartWithDetails.displayName = 'DonutChartWithDetails';
