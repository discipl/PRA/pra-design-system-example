import ReadMe from '@persoonlijke-regelingen-assistent/components-css/DonutChartWithDetails/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { DonutChartWithDetails } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof DonutChartWithDetails> = {
  component: DonutChartWithDetails,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/SpecificComponents/DonutChartWithDetails',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'donutchartwithdetails',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof DonutChartWithDetails>;

const NogTeBetalenInkomstenbelasting = 8264;
const LoonheffingTotaal = 14924;
const Inkomstenbelasting = 23188;
const AowBedrag = 18031;
const Fonds1 = 10795;
const Fonds2 = 50000;
const Fonds1Label = 'Pensioenfonds 1';
const Fonds2Label = 'Pensioenfonds 2';
const BrutoPensioenTotaal = 78826;
const PotjesTotaal = 60795;

export const Default: Story = {
  args: {
    HeaderAmount: {
      Label: 'Schatting nog te betalen bedrag door u',
      Color: 'var(--pra-color-secondary-300)',
      AmountType: 'Single',
      Amount: NogTeBetalenInkomstenbelasting,
    },

    Calculations: [
      {
        Label: 'Inkomen',
        CalculationType: 'Summation',
        Amounts: [
          {
            Label: 'Pensioenuitkeringen',
            Color: 'var(--pra-color-secondary-200)',
            AmountType: 'Total',
            Parts: {
              Values: [Fonds1, Fonds2],
              Labels: [Fonds1Label, Fonds2Label],
            },
            Amount: PotjesTotaal,
          },
          {
            Label: 'AOW',
            Color: 'var(--pra-color-secondary-600)',
            AmountType: 'Single',
            Amount: AowBedrag,
          },
          {
            Label: 'Overig inkomen',
            AmountType: 'Unknown',
          },
        ],
        TotalAmount: {
          Label: 'Totaal bruto-inkomen op jaarbasis',
          AmountType: 'Single',
          Amount: BrutoPensioenTotaal,
        },
      },
      {
        Label: 'Inkomstenbelasting',
        CalculationType: 'Substraction',
        TotalAmount: {
          Label: 'Totaal verschuldigde inkomstenbelasting',
          AmountType: 'Single',
          Amount: Inkomstenbelasting,
        },
        Amounts: [
          {
            Label: 'Loonheffing',
            Color: 'var(--pra-color-secondary-400)',
            AmountType: 'Single',
            Amount: LoonheffingTotaal,
          },
        ],
        ResultingAmount: {
          Label: 'Schatting nog te betalen bedrag',
          Color: 'var(--pra-color-secondary-300)',
          AmountType: 'Single',
          Amount: NogTeBetalenInkomstenbelasting,
        },
      },
    ],
  },
};
