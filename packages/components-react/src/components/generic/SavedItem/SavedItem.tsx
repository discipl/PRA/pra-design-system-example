import alertCircle from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/AlertCircle';
import ArrowRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ArrowRight';
import '@persoonlijke-regelingen-assistent/components-css/SavedItem/SavedItem.scss';
import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { Button } from '../Button';
import { Heading } from '../Heading';
import { Icon } from '../Icon';
import { Paragraph } from '../Paragraph';

interface SavedItemSpecificProps {
  title?: string;
  message?: string;
  notificationMessage?: string;
  savedItemClickAction: Function;
}

export interface SavedItemProps extends SavedItemSpecificProps {}

export const SavedItem: React.ForwardRefExoticComponent<
  SavedItemProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { title, message, notificationMessage, savedItemClickAction }: PropsWithChildren<SavedItemProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className="pra-saved-item">
        <Heading level={3}>{title}</Heading>
        <Paragraph>{message}</Paragraph>
        <div className="pra-saved-item-notification">
          {notificationMessage && [<Icon>{alertCircle({})}</Icon>, <Paragraph>{notificationMessage}</Paragraph>]}
          <Button appearance="subtle-button" onClick={() => savedItemClickAction()}>
            Bekijk <Icon>{ArrowRight({})}</Icon>
          </Button>
        </div>
      </div>
    );
  },
);

SavedItem.displayName = 'SavedItem';
