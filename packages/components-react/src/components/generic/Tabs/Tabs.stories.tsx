import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Tabs/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Tabs } from './index';
import { Image } from '../Image';
import { Paragraph } from '../Paragraph';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof Tabs> = {
  component: Tabs,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Tabs',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'tabs',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Tabs>;

export const Default: Story = {
  args: {
    tabData: [
      {
        label: 'Openstaand',
        panelContent: <Paragraph>De openstaande taken</Paragraph>,
      },
      {
        label: 'Voltooid',
        panelContent: <Image className="utrecht-img--scale-down" src={birthday_party} alt="Birthday Party" />,
      },
    ],
  },
};
