import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Currency/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Currency } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Currency> = {
  component: Currency,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Currency',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'currency',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Currency>;

export const Default: Story = {
  args: {
    currencySymbol: '€',
  },
};

export const Dollar: Story = {
  args: {
    ...Default.args,
    currencySymbol: '$',
  },
};
