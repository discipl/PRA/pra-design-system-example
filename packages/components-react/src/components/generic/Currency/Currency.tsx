import { ForwardedRef, forwardRef, InputHTMLAttributes, PropsWithChildren } from 'react';
import { Textbox } from '../Textbox';
import '@persoonlijke-regelingen-assistent/components-css/Currency/Currency.scss';

interface CurrencySpecificProps {
  currencySymbol?: string;
}
export interface CurrencyProps extends InputHTMLAttributes<HTMLInputElement>, CurrencySpecificProps {}

export const Currency: React.ForwardRefExoticComponent<
  CurrencyProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ currencySymbol, ...otherProps }: PropsWithChildren<CurrencyProps>, ref: ForwardedRef<HTMLDivElement>) => {
    const { required } = otherProps as InputHTMLAttributes<HTMLInputElement>;
    return (
      <div className="pra-currency" data-currencySymbol={currencySymbol} ref={ref}>
        <Textbox
          {...(otherProps as InputHTMLAttributes<HTMLInputElement>)}
          aria-required={required}
          type="number"
          invalid={false}
        />
      </div>
    );
  },
);

Currency.displayName = 'Currency';
