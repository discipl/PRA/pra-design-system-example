import '@persoonlijke-regelingen-assistent/components-css/NotificationReminder/NotificationReminder.scss';
import React, { ForwardedRef, forwardRef, PropsWithChildren, SVGProps } from 'react';
import { Heading } from '../Heading';
import { Paragraph } from '../Paragraph';

interface NotificationReminderSpecificProps {
  title?: string;
  message?: string;
  // eslint-disable-next-line no-unused-vars
  icon?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
}

export interface NotificationReminderProps extends NotificationReminderSpecificProps {}

export const NotificationReminder: React.ForwardRefExoticComponent<
  NotificationReminderProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ icon, title, message }: PropsWithChildren<NotificationReminderProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-notification-reminder">
        <div className="pra-notification-reminder-title">
          <span className="pra-notification-reminder-icon">{icon && icon({})}</span>
          <Heading level={3}>{title}</Heading>
        </div>
        <Paragraph>{message}</Paragraph>
        <ul className="pra-notification-reminder-buttons">
          <li>Herinner mij later</li>
          <li>Naar melding</li>
          <li>Ja</li>
        </ul>
      </div>
    );
  },
);

NotificationReminder.displayName = 'NotificationReminder';
