import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import ChevronUp from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronUp';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, useState } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Dropdown/Dropdown.scss';

interface DropdownSpecificProps {
  title?: string;
  initCollapsed?: boolean;
  variant: 'Default' | 'No Line' | 'Stappenplan';
}

export interface DropdownProps extends DropdownSpecificProps, React.HTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  ref?: ForwardedRef<HTMLDivElement>;
}

export const Dropdown: React.ForwardRefExoticComponent<DropdownProps> = forwardRef(
  (
    {
      title = '',
      initCollapsed = true,
      variant = 'No Line',
      className,
      ...otherProps
    }: PropsWithChildren<DropdownProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const { children } = otherProps as HTMLAttributes<HTMLInputElement>;
    const [collapsed, setCollapsed] = useState<boolean>(initCollapsed);
    const hasCircle = ['Stappenplan', 'No Line'].includes(variant);
    return (
      <div className={`pra-dropdown ${className ? className : ''}`} ref={ref} {...otherProps}>
        <div
          className={`pra-dropdown-header ${hasCircle ? 'pra-dropdown-header-with-circle' : ''}`}
          onClick={() => {
            setCollapsed(!collapsed);
          }}
        >
          {hasCircle && (
            <div className="pra-dropdown-header--stappenplan-circle-container">
              <div className="pra-dropdown-header--stappenplan-circle"></div>
            </div>
          )}
          <h3 className="pra-dropdown-title">{title}</h3>
          {!collapsed && ChevronUp({})}
          {!!collapsed && ChevronDown({})}
        </div>
        {!collapsed && (
          <div
            className={`pra-dropdown-content ${
              hasCircle && `pra-dropdown-content--${variant.split(' ').join('-').toLowerCase()}`
            } `}
          >
            {children}
          </div>
        )}
      </div>
    );
  },
);

Dropdown.displayName = 'Dropdown';
