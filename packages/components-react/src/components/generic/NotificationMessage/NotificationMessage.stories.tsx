import ReadMe from '@persoonlijke-regelingen-assistent/components-css/NotificationMessage/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { NotificationMessage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof NotificationMessage> = {
  component: NotificationMessage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/NotificationMessage',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'notificationMessage',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof NotificationMessage>;

export const Default: Story = {
  args: {
    buttonLabel: 'Check het nu',
    notificationTitle: 'Heb je je zorgtoeslag al aangevraagd?',
    message:
      'Lorem ipsum dolor sit amet consectetur. Leo a sit eget tincidunt convallis. Tortor pretium mauris posuere amet egestas morbi ultricies vestibulum faucibus.',
    buttonHandler: () => {
      console.log('Button Clicked');
    },
  },
};
