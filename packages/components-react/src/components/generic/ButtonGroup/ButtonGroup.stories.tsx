import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ButtonGroup/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ButtonGroup } from './index';
import { Button } from '../Button';
import { ButtonBadge } from '../ButtonBadge';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof ButtonGroup> = {
  component: ButtonGroup,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/ButtonGroup',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'buttongroup',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ButtonGroup>;

export const Default: Story = {
  args: {
    children: [<Button>A</Button>, <Button>B</Button>],
  },
};
export const ButtonBadgesGroup: Story = {
  args: {
    children: [
      <ButtonBadge appearance="primary">Thuiswonend</ButtonBadge>,
      <ButtonBadge appearance="primary">Koophuis</ButtonBadge>,
      <ButtonBadge appearance="primary">Huurwoning</ButtonBadge>,
      <ButtonBadge appearance="primary">Studentenwoning</ButtonBadge>,
      <ButtonBadge appearance="primary">Iets anders</ButtonBadge>,
    ],
  },
};

export const ButtonBadgeGroupInSmallParent: Story = {
  args: {
    ...ButtonBadgesGroup.args,
  },
  decorators: [
    (Story) => (
      <div style={{ width: '400px', border: '1px solid black', padding: '5px' }}>
        <Story />
      </div>
    ),
  ],
};
