import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Icon/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Icon } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof Icon> = {
  component: Icon,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Icon',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'icon',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Icon>;

export const FullSize: Story = {
  args: {
    children: [layout_grid({})],
  },
};

export const Large: Story = {
  args: {
    children: [layout_grid({ style: { width: '128px' } })],
  },
};
export const Medium: Story = {
  args: {
    children: [layout_grid({ style: { width: '48px' } })],
  },
};

export const Small: Story = {
  args: {
    children: [layout_grid({ style: { width: '24px' } })],
  },
};
