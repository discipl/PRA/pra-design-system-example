/**
 * @license EUPL-1.2
 * Copyright (c) 2021 Robbert Broersma
 */

export { Link } from '@utrecht/component-library-react/dist/css-module';
