import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Link/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Link } from './index';
import { Document } from '../Document';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Link> = {
  component: Link,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Link',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'link',
    ['--pra-component-design-version-name']: '0.0.0',
  },
  decorators: [
    (Story) => (
      <Document>
        <Story />
      </Document>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof Link>;

export const Default: Story = {
  args: {
    href: '#',
    children: ['LinkText'],
  },
};
