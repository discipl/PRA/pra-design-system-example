import ReadMe from '@persoonlijke-regelingen-assistent/components-css/FormToggle/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { FormToggle } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof FormToggle> = {
  component: FormToggle,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/FormToggle',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'formtoggle',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof FormToggle>;

export const Default: Story = {
  args: {
    id: 'd26299c0-d61d-460b-9278-4adab8440a28',
    invalid: false,
  },
};

export const Invalid: Story = {
  args: {
    ...Default.args,
    invalid: true,
  },
};
