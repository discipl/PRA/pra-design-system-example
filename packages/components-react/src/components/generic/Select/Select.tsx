import { Select as UtrechtSelect, SelectProps as UtrechtSelectProps } from '@utrecht/component-library-react';
import React, { ForwardedRef, forwardRef, InputHTMLAttributes, PropsWithChildren } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Select/Select.scss';

interface SelectSpecificProps {
  iconStart?: JSX.Element;
  iconEnd?: JSX.Element;
  invalid?: UtrechtSelectProps['invalid'];
  busy?: UtrechtSelectProps['busy'];
  noscript?: UtrechtSelectProps['noscript'];
}
export interface SelectProps extends PropsWithChildren<SelectSpecificProps>, InputHTMLAttributes<HTMLSelectElement> {}

export const Select: React.ForwardRefExoticComponent<SelectProps & React.RefAttributes<HTMLLabelElement>> = forwardRef(
  ({ ...props }: SelectProps, ref: ForwardedRef<HTMLLabelElement>) => {
    const { iconStart, iconEnd, id, children, ...otherProps } = props;
    return (
      <label className="pra-select" ref={ref} htmlFor={id}>
        {iconStart && <span className="pra-icon pra-icon-start">{iconStart}</span>}
        <UtrechtSelect {...otherProps} id={id}>
          {children}
        </UtrechtSelect>
        {iconEnd && <span className="pra-icon pra-icon-end">{iconEnd}</span>}
      </label>
    );
  },
);

Select.displayName = 'Select';
