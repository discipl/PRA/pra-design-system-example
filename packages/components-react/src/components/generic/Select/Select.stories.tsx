import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Select } from './index';
import { SelectOption } from '../SelectOption';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Select> = {
  component: Select,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Select',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'Select',
  },
};

export default meta;
type Story = StoryObj<typeof Select>;

export const Default: Story = {
  args: {
    children: [
      <SelectOption value="option1" label="label1" disabled={false} />,
      <SelectOption value="option2" label="label2" disabled={false} />,
      <SelectOption value="option3" label="label3" disabled={false} />,
    ],
    invalid: false,
    disabled: false,
    required: false,
  },
};

export const WithStartIcon: Story = {
  args: {
    ...Default.args,
    iconStart: Search({}),
  },
};
export const WithEndIcon: Story = {
  args: {
    ...Default.args,
    iconEnd: ChevronDown({}),
  },
};
export const WithTwoIcons: Story = {
  args: {
    ...Default.args,
    iconStart: Search({}),
    iconEnd: ChevronDown({}),
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
  },
};
export const DisabledWithIcon: Story = {
  args: {
    ...Default.args,
    disabled: true,
    iconEnd: ChevronDown({}),
  },
};

export const Required: Story = {
  args: {
    ...Default.args,
    required: true,
  },
};
export const Invalid: Story = {
  args: {
    ...Default.args,
    invalid: true,
  },
};
export const InvalidWithIcon: Story = {
  args: {
    ...Default.args,
    invalid: true,
    iconEnd: ChevronDown({}),
  },
};
export const WithStaticValue: Story = {
  args: {
    ...Default.args,
    value: 'option1',
  },
};
