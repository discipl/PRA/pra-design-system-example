import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Accordeon/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Accordeon } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Accordeon> = {
  component: Accordeon,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/Accordeon',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'accordeon',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Accordeon>;

export const Collapsed: Story = {
  parameters: {
    docs: {
      description: {
        story: "Accordeon when it's status is <strong>collapsed</strong>",
      },
    },
  },
  args: {
    initCollapsed: true,
    title: 'Inkomenscomponenten',
    children: [
      <table>
        <tr>
          <td>Pensieonuitkering</td>
          <td>€60.795</td>
        </tr>
        <tr>
          <td>AOW</td>
          <td>€18.031</td>
        </tr>
      </table>,
    ],
  },
};

export const Expanded: Story = {
  parameters: {
    docs: {
      description: {
        story: "Accordeon when it's status is <strong>expanded</strong>",
      },
    },
  },
  args: {
    ...Collapsed.args,
    initCollapsed: false,
  },
};
