import ReadMe from '@persoonlijke-regelingen-assistent/components-css/DataList/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { DataList } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof DataList> = {
  component: DataList,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/DataList',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'datalist',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof DataList>;

export const Default: Story = {
  args: {
    title: 'Contact gegevens',
    items: [
      { key: 'Adres', value: 'Kerkstraat 1' },
      { key: 'Woonplaats', value: 'Amsterdam' },
    ],
  },
};
