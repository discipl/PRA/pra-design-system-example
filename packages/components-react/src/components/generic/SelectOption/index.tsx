/**
 * @license EUPL-1.2
 * Copyright (c) 2021 Robbert Broersma
 */

export { SelectOption } from '@utrecht/component-library-react/dist/css-module';
