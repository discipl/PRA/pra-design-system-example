import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { SelectOption } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SelectOption> = {
  component: SelectOption,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SelectOption',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'SelectOption',
  },
};

export default meta;
type Story = StoryObj<typeof SelectOption>;

export const Default: Story = {
  args: {
    value: 'option1',
    label: 'label1',
    disabled: false,
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
  },
};
