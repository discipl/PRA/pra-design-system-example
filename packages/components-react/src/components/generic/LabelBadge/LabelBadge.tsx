import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/LabelBadge/LabelBadge.scss';

interface LabelBadgeSpecificProps {
  variant: 'primary' | 'secondary';
}
export interface LabelBadgeProps extends HTMLAttributes<HTMLInputElement>, LabelBadgeSpecificProps {}

export const LabelBadge: React.ForwardRefExoticComponent<
  LabelBadgeProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDataElement>
> = forwardRef(
  ({ variant = 'primary', ...props }: PropsWithChildren<LabelBadgeProps>, ref: ForwardedRef<HTMLDataElement>) => {
    const { children } = props;
    return (
      <span
        ref={ref}
        {...props}
        className={`${props.className ? props.className : ''} pra-label-badge pra-label-badge--${variant}`}
      >
        {children}
      </span>
    );
  },
);

LabelBadge.displayName = 'LabelBadge';
