import check from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Check';
import React, { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/ProgressTracker/ProgressTracker.scss';

interface ProgressTrackerSpecificProps {
  total?: number;
  progress?: number;
}
export interface ProgressTrackerProps extends ProgressTrackerSpecificProps {}

export const ProgressTracker: React.ForwardRefExoticComponent<
  ProgressTrackerProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ total = 1, progress = 0 }: PropsWithChildren<ProgressTrackerProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-progress-container">
        <div className="pra-progress">
          <hr />
          <div className="pra-dots">
            {[...Array(total).keys()].map((i) => {
              if (i === progress - 1) {
                return <span className="pra-dot pra-dot-filled" key={`dot-${i}`} />;
              } else if (i < progress - 1) {
                return (
                  <span className="pra-dot pra-dot-filled pra-dot-checked" key={`dot-${i}`}>
                    {check({})}
                  </span>
                );
              }
              return <span className="pra-dot" key={`dot-${i}`} />;
            })}
          </div>
        </div>
      </div>
    );
  },
);

ProgressTracker.displayName = 'ProgressTracker';
