import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/SpeachBulb/SpeachBulb.scss';

interface SpeachBulbSpecificProps {
  shown: boolean;
}

export interface SpeachBulbProps
  extends SpeachBulbSpecificProps,
    HTMLAttributes<HTMLDivElement>,
    React.RefAttributes<HTMLDivElement> {}

export const SpeachBulb: React.ForwardRefExoticComponent<
  SpeachBulbProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { children, shown, className, ...otherProps }: PropsWithChildren<SpeachBulbProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return shown ? (
      <div ref={ref} className={`pra-speach-bulb ${className}`} {...otherProps}>
        {children}
      </div>
    ) : null;
  },
);

SpeachBulb.displayName = 'SpeachBulb';
