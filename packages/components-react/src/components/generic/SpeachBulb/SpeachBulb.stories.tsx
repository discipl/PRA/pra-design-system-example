import ReadMe from '@persoonlijke-regelingen-assistent/components-css/SpeachBulb/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { SpeachBulb } from './index';
import { NotificationMessage } from '../NotificationMessage';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SpeachBulb> = {
  component: SpeachBulb,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SpeachBulb',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'speachbulb',
    ['--pra-component-design-version-name']: '0.0.0',
  },
  decorators: [
    (Story) => (
      <div style={{ backgroundColor: '#F0F2F6', padding: '20px' }}>
        <Story />
      </div>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof SpeachBulb>;

export const Default: Story = {
  args: { children: [<strong>Hulp nodig?</strong>] },
};

export const Reminder: Story = {
  args: {
    children: [
      <NotificationMessage
        buttonHandler={() => {}}
        notificationTitle="Heb je je zorgtoeslag al aangevraagd?"
        message="Lorem ipsum dolor sit amet consectetur. Leo a sit eget tincidunt convallis. Tortor pretium mauris posuere amet egestas morbi ultricies vestibulum faucibus."
        buttonLabel="Vraag aan"
      />,
    ],
  },
};
