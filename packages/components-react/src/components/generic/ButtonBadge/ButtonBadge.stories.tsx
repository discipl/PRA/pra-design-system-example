import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ButtonBadge } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof ButtonBadge> = {
  component: ButtonBadge,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/ButtonBadge',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'buttonbadge',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ButtonBadge>;

export const Default: Story = {
  args: {
    pressed: false,
    appearance: 'primary',
    children: ['Ik'],
  },
};

export const Pressed: Story = {
  args: {
    ...Default.args,
    pressed: true,
  },
};

export const Secondary: Story = {
  args: {
    ...Default.args,
    appearance: 'secondary',
  },
};

export const SecondaryPressed: Story = {
  args: {
    ...Secondary.args,
    pressed: true,
  },
};
