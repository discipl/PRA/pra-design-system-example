import React, { ButtonHTMLAttributes, ForwardedRef, forwardRef } from 'react';
import { Button } from '../Button';

import '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/ButtonBadge.scss';

interface ButtonBadgeSpecificProps {
  pressed?: boolean;
  appearance: 'primary' | 'secondary' | 'tertiary';
}
export interface ButtonBadgeProps extends ButtonHTMLAttributes<HTMLButtonElement>, ButtonBadgeSpecificProps {}

export const ButtonBadge: React.ForwardRefExoticComponent<ButtonBadgeProps & React.RefAttributes<HTMLButtonElement>> =
  forwardRef(({ ...props }: ButtonBadgeProps, ref: ForwardedRef<HTMLButtonElement>) => {
    const { children, pressed, appearance = 'primary', ...otherProps } = props;
    return (
      <Button
        ref={ref}
        className="pra-button-badge"
        appearance={`${appearance}-action-button`}
        pressed={pressed}
        {...otherProps}
      >
        {children}
      </Button>
    );
  });

ButtonBadge.displayName = 'ButtonBadge';
