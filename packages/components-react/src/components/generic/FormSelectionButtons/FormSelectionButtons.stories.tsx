import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ButtonBadge/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { FormSelectionButtons } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof FormSelectionButtons> = {
  component: FormSelectionButtons,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/FormSelectionButtons',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'formSelectionButtons',
    ['--pra-component-design-version-name']: 'autosave - 4661964929',
    ['--pra-component-design-version-id']: '4661964929',
  },
};

export default meta;
type Story = StoryObj<typeof FormSelectionButtons>;

export const Default: Story = {
  args: {
    options: [
      { value: 'option1', label: 'label1' },
      { value: 'option2', label: 'label2' },
    ],
    onValueChange: (value) => {
      console.log(value);
    },
  },
};

export const WithInitialValue: Story = {
  args: {
    ...Default.args,
    initialValue: 'option2',
  },
};
