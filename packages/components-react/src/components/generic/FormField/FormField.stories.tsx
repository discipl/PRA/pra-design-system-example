import ReadMe from '@persoonlijke-regelingen-assistent/components-css/FormField/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { FormField } from './index';
import { Checkbox } from '../Checkbox';
import { Textbox } from '../Textbox';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof FormField> = {
  component: FormField,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/FormField',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'formField',
    ['--pra-component-design-version-name']: 'autosave - 4661964929',
    ['--pra-component-design-version-id']: '4661964929',
  },
};

export default meta;
type Story = StoryObj<typeof FormField>;

export const TextboxField: Story = {
  args: {
    label: 'Gemeente',
    type: 'textbox',
    children: [<Textbox placeholder="Type hier..." invalid={false} />],
  },
};

export const CheckboxField: Story = {
  args: {
    label: 'Optie a',
    type: 'checkbox',
    children: [<Checkbox checked={true} invalid={false} />],
  },
};
