import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Button/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Button } from './index';
import { Icon } from '../Icon';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof Button> = {
  component: Button,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Button',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'button',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Button>;

export const Default: Story = {
  args: {
    children: ['Button label'],
  },
};
export const Primary: Story = {
  args: {
    appearance: 'primary-action-button',
    children: ['Button label'],
  },
};

export const PrimaryDisabled: Story = {
  args: {
    appearance: 'primary-action-button',
    disabled: true,
    children: ['Button label'],
  },
};
export const PrimaryPressed: Story = {
  args: {
    appearance: 'primary-action-button',
    pressed: true,
    children: ['Button label'],
  },
};
export const PrimaryBusy: Story = {
  args: {
    appearance: 'primary-action-button',
    busy: true,
    children: ['Button label'],
  },
};

export const PrimaryWithIcon: Story = {
  args: {
    appearance: 'primary-action-button',
    children: [<Icon>{ChevronRight({ fill: 'var(--_utrecht-button-color)' })}</Icon>, 'Button label'],
  },
};
export const Secondary: Story = {
  args: {
    appearance: 'secondary-action-button',
    children: ['Button label'],
  },
};

export const SecondaryDisabled: Story = {
  args: {
    appearance: 'secondary-action-button',
    disabled: true,
    children: ['Button label'],
  },
};
export const SecondaryPressed: Story = {
  args: {
    appearance: 'secondary-action-button',
    pressed: true,
    children: ['Button label'],
  },
};
export const SecondaryBusy: Story = {
  args: {
    appearance: 'secondary-action-button',
    busy: true,
    children: ['Button label'],
  },
};

export const SecondaryWithIcon: Story = {
  args: {
    appearance: 'secondary-action-button',
    children: [<Icon>{ChevronRight({ fill: 'var(--_utrecht-button-color)' })}</Icon>, 'Button label'],
  },
};

export const Subtile: Story = {
  args: {
    appearance: 'subtle-button',
    children: ['Button label'],
  },
};

export const SubtileDisabled: Story = {
  args: {
    appearance: 'subtle-button',
    disabled: true,
    children: ['Button label'],
  },
};
export const SubtilePressed: Story = {
  args: {
    appearance: 'subtle-button',
    pressed: true,
    children: ['Button label'],
  },
};
export const SubtileBusy: Story = {
  args: {
    appearance: 'subtle-button',
    busy: true,
    children: ['Button label'],
  },
};

export const SubtileWithIcon: Story = {
  args: {
    appearance: 'subtle-button',
    children: [<Icon>{ChevronRight({ fill: 'var(--_utrecht-button-color)' })}</Icon>, 'Button label'],
  },
};
