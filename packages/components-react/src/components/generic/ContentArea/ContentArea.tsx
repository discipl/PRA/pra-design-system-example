import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/ContentArea/ContentArea.scss';

interface ContentAreaSpecificProps {}
export interface ContentAreaProps extends HTMLAttributes<HTMLInputElement>, ContentAreaSpecificProps {}

export const ContentArea: React.ForwardRefExoticComponent<
  ContentAreaProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ ...props }: PropsWithChildren<ContentAreaProps>, ref: ForwardedRef<HTMLDivElement>) => {
  const { children, ...otherProps } = props;
  return (
    <div className="pra-content-area" ref={ref} {...otherProps}>
      {children}
    </div>
  );
});

ContentArea.displayName = 'ContentArea';
