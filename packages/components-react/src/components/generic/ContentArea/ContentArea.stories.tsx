import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ContentArea/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ContentArea } from './index';
import { UnorderedList } from '../UnorderedList';
import { UnorderedListItem } from '../UnorderedListItem';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof ContentArea> = {
  component: ContentArea,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/ContentArea',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'contentarea',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ContentArea>;

export const Default: Story = {
  args: {
    children: [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pharetra consectetur nunc ac pretium. Integer suscipit in nisi sed egestas. Fusce fermentum, leo ut ultricies ullamcorper, ante nisl fermentum magna, ut luctus dolor massa quis est. Morbi facilisis euismod velit id molestie. Maecenas tellus orci, volutpat ac dapibus sed, luctus vitae eros. Vivamus ac rutrum sem, id interdum magna. Nullam sit amet semper augue. Cras in sem elit.',
    ],
  },
};
const UnorderedListItemComponent = <UnorderedListItem markerContent=">">Item</UnorderedListItem>;

export const WithList: Story = {
  args: {
    children: [<UnorderedList>{Array(5).fill(UnorderedListItemComponent)}</UnorderedList>],
  },
};
