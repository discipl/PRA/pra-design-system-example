import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Fieldset/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Fieldset } from './index';
import { FormLabel } from '../FormLabel';
import { Textbox } from '../Textbox';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Fieldset> = {
  component: Fieldset,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Fieldset',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'formField',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Fieldset>;

export const Default: Story = {
  args: {
    children: [<FormLabel>Gemeente:</FormLabel>, <Textbox placeholder="Type hier..." invalid={false} />],
  },
};
