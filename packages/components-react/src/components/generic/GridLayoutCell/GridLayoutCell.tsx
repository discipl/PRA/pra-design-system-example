import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/GridLayoutCell/GridLayoutCell.scss';

interface GridLayoutCellSpecificProps {
  columnSpan?: number;
  justifyContent?: 'start' | 'center' | 'end';
  alignItems?: 'start' | 'center' | 'end';
  paddingInline?: number;
}

export interface GridLayoutCelldProps extends HTMLAttributes<HTMLDivElement>, GridLayoutCellSpecificProps {}

export const GridLayoutCell: React.ForwardRefExoticComponent<
  GridLayoutCelldProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      children,
      columnSpan = 1,
      justifyContent = 'center',
      alignItems = 'start',
      paddingInline = 20,
      ...otherProps
    }: PropsWithChildren<GridLayoutCelldProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div
        {...otherProps}
        ref={ref}
        style={
          {
            '--pra-grid-layout-cell-column-end': `span ${columnSpan}`,
            '--pra-grid-layout-cell-justify-content': justifyContent,
            '--pra-grid-layout-cell-align-items': alignItems,
            '--pra-grid-layout-cell-padding-inline': `${paddingInline}px`,
            ...otherProps?.style,
          } as React.CSSProperties
        }
        className="pra-grid-layout-cell"
      >
        {children}
      </div>
    );
  },
);

GridLayoutCell.displayName = 'GridLayoutCell';
