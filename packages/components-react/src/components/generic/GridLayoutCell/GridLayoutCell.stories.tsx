import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/GridLayoutCell/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { GridLayoutCell } from './index';
import { Image } from '../Image';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof GridLayoutCell> = {
  component: GridLayoutCell,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/GridLayoutCell',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'gridlayoutcell',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof GridLayoutCell>;

export const JustifyAndAlignStart: Story = {
  args: {
    justifyContent: 'start',
    alignItems: 'start',
    paddingInline: 20,
    columnSpan: 3,
    children: [<Image src={birthday_party} className="utrecht-img--scale-down" alt="Birthday Party" />],
  },
};
export const JustifyAndAlignEnd: Story = {
  args: {
    justifyContent: 'end',
    alignItems: 'end',
    paddingInline: 0,
    columnSpan: 3,
    children: [<Image src={birthday_party} className="utrecht-img--scale-down" alt="Birthday Party" />],
  },
};
export const JustifyAndAlignCenter: Story = {
  args: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingInline: 300,
    columnSpan: 3,
    children: [<Image src={birthday_party} className="utrecht-img--scale-down" alt="Birthday Party" />],
  },
};
