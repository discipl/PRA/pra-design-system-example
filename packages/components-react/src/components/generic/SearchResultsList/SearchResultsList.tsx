import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/SearchResultsList/SearchResultsList.scss';

interface SearchResultsListSpecificProps {}
export interface SearchResultsListProps extends SearchResultsListSpecificProps {}

export const SearchResultsList: React.ForwardRefExoticComponent<
  SearchResultsListProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLOListElement>
> = forwardRef(({ children }: PropsWithChildren<SearchResultsListProps>, ref: ForwardedRef<HTMLOListElement>) => {
  return (
    <ol ref={ref} className="pra-search-results-list">
      {children}
    </ol>
  );
});

SearchResultsList.displayName = 'SearchResultsList';
