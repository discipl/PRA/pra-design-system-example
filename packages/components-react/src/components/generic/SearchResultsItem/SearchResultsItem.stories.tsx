import ReadMe from '@persoonlijke-regelingen-assistent/components-css/SearchResultsItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { SearchResultsItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SearchResultsItem> = {
  component: SearchResultsItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SearchResultsItem',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'searchresultsitem',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof SearchResultsItem>;

export const Default: Story = {
  args: {
    title: 'Titel Regeling',
    subTitle: 'Overheidsinstantie',
    description:
      '01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt.',
  },
};

export const LongTitle: Story = {
  args: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    subTitle: 'Overheidsinstantie',
    description:
      '01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt.',
  },
};
