import React, { ForwardedRef, forwardRef, PropsWithChildren, SVGProps } from 'react';
import { Image } from '../Image';
import { Paragraph } from '../Paragraph';
import '@persoonlijke-regelingen-assistent/components-css/InfoSlide/InfoSlide.scss';

interface InfoSlideSpecificProps {
  information?: string;
  imageSrc?: string;
  imageAlt?: string;
  // eslint-disable-next-line no-unused-vars
  backgroundVector?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
}

export interface InfoSlideProps extends InfoSlideSpecificProps {}

export const InfoSlide: React.ForwardRefExoticComponent<
  InfoSlideProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { information = '', imageAlt = '', imageSrc = '', backgroundVector }: PropsWithChildren<InfoSlideProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className="pra-info-slide">
        <div className="pra-info-slide-background-container">
          {!!backgroundVector && backgroundVector({ className: 'pra-info-slide-background-vector' })}
        </div>
        <Image className="utrecht-img--scale-down" src={imageSrc} alt={imageAlt} />
        <Paragraph>{information}</Paragraph>
      </div>
    );
  },
);

InfoSlide.displayName = 'InfoSlide';
