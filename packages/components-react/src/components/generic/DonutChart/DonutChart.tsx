import { Pie } from '@visx/shape';
import { ProvidedProps } from '@visx/shape/lib/shapes/Pie';
import { CSSProperties, ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { ColorSample } from '../ColorSample';
import '@persoonlijke-regelingen-assistent/components-css/DonutChart/DonutChart.scss';
interface ElementData {
  key: string;
  value: number;
  isSelected?: boolean;
  stroke: string;
  fill: string;
}
type DataType = ElementData[];

export interface DonutChartProps {
  radius: number;
  arcWidth: number;
  data: DataType;
  showLabels: boolean;
  donutValue: string;
}
export interface DonutChartArcProps {
  donutChart: ProvidedProps<ElementData>;
  arc: {
    data: ElementData;
    endAngle: number;
    index: number;
    padAngle: number;
    startAngle: number;
    value: number;
  };
  index: number;
  arcFill: string;
  showLabel: boolean;
  arcStroke: string;
}

export interface DonutChartArcLabelProps {
  centroidX: number;
  centroidY: number;
  value: number;
}

function Arc({ donutChart, arc, index, arcFill, arcStroke }: DonutChartArcProps) {
  const { key, value, isSelected } = arc.data;
  const [centroidX, centroidY] = donutChart.path.centroid(arc);
  const arcPath = donutChart.path(arc) || undefined;
  const hasSpaceForLabel = arc.endAngle - arc.startAngle >= 0.1;
  const showLabel = false;
  return (
    <g key={`arc-${key}-${index}`}>
      <path
        d={arcPath}
        fill={arcFill}
        stroke={arcStroke}
        className={`pra-donutchart-arc ${isSelected ? 'pra-donutchart-arc-selected' : ''}`}
      />
      {showLabel && hasSpaceForLabel && <ArcLabel centroidX={centroidX} centroidY={centroidY} value={value} />}
    </g>
  );
}

function ArcLabel({ centroidX, centroidY, value }: DonutChartArcLabelProps) {
  return (
    <text x={centroidX} y={centroidY} dy=".33em" className="pra-donutchart-arc-label">
      {value}
    </text>
  );
}

export const DonutChart: React.ForwardRefExoticComponent<
  DonutChartProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { data = [], arcWidth = 60, radius = 320, showLabels, donutValue }: PropsWithChildren<DonutChartProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const value = (d: ElementData) => d.value;
    const sortedData = data.sort((arc1, arc2) => {
      return arc1.isSelected === arc2.isSelected ? 0 : arc1.isSelected ? 1 : -1;
    });

    return (
      <div
        ref={ref}
        className="pra-donutchart"
        style={{ '--pra-donutchart-radius-size': `${radius}px` } as CSSProperties}
      >
        <svg className="pra-donutchart-chart">
          <g>
            <Pie
              data={sortedData}
              pieValue={value}
              pieSortValues={() => 1}
              outerRadius={radius / 2}
              innerRadius={radius / 2 - arcWidth}
              startAngle={Math.PI * 4.5}
            >
              {(donutChart: ProvidedProps<ElementData>) => {
                return donutChart.arcs.map((arc, index) => {
                  return (
                    <Arc
                      donutChart={donutChart}
                      arc={arc}
                      index={index}
                      arcFill={arc.data.fill}
                      arcStroke={arc.data.stroke}
                      showLabel={showLabels}
                    />
                  );
                });
              }}
            </Pie>
            <text className="pra-donutchart-label">{donutValue}</text>
          </g>
        </svg>
        <ul className="pra-donutchart-legend">
          {sortedData.slice().map((arcData) => {
            return (
              <li>
                <ColorSample color={arcData.fill} />
                <label>{arcData.key}</label>
              </li>
            );
          })}
        </ul>
      </div>
    );
  },
);

DonutChart.displayName = 'DonutChart';
