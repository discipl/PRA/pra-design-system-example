import ReadMe from '@persoonlijke-regelingen-assistent/components-css/DonutChart/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { DonutChart } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof DonutChart> = {
  component: DonutChart,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/DonutChart',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'donutchart',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof DonutChart>;

export const Default: Story = {
  args: {
    radius: 200,
    arcWidth: 45,
    data: [
      {
        key: 'Te betalen bedrag',
        value: 10,
        isSelected: true,
        fill: 'var(--pra-color-secondary-600)',
        stroke: '#1D1D66',
      },
      { key: 'Eerdere VA *', value: 30, fill: '#BCDCFF', stroke: '#445577' },
      { key: 'Ingehouden loonheffing', value: 30, fill: '#FFC989', stroke: '#776633' },
      { key: 'AOW', value: 20, fill: '#0D2745', stroke: '#000811' },
      { key: 'Pensioen uitkering', value: 10, fill: '#A4A4FF', stroke: '#444477' },
    ],
    showLabels: false,
    donutValue: '€ 1000',
  },
};
export const BDInkomstenBelasting: Story = {
  args: {
    radius: 200,
    arcWidth: 45,
    data: [
      {
        key: 'Loonheffingen',
        value: 14188,
        fill: 'var(--pra-color-secondary-200)',
        stroke: 'var(--pra-color-secondary-200)',
      },
      {
        key: 'Schatting nog te betalen bedrag',
        value: 9000,
        fill: 'var(--pra-color-secondary-600)',
        stroke: 'var(--pra-color-secondary-600)',
      },
    ],
    showLabels: false,
    donutValue: '€ 23.188',
  },
};
export const BDInkomen: Story = {
  args: {
    radius: 200,
    arcWidth: 45,
    data: [
      { key: 'AOW', value: 30826, fill: 'var(--pra-color-tertiary-200)', stroke: 'var(--pra-color-tertiary-200)' },
      {
        key: 'Pensioen uitkering',
        value: 48000,
        fill: 'var(--pra-color-primary-300)',
        stroke: 'var(--pra-color-primary-300)',
      },
    ],
    showLabels: false,
    donutValue: '€ 78.826',
  },
};
