import ReadMe from '@persoonlijke-regelingen-assistent/components-css/GridLayout/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { GridLayout } from './index';
import { GridLayoutCell } from '../GridLayoutCell';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof GridLayout> = {
  component: GridLayout,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/GridLayout',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'gridlayout',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof GridLayout>;

export const SpanEntireRow: Story = {
  args: {
    templateColumns: 6,
    children: [
      <GridLayoutCell columnSpan={6} justifyContent="center">
        <div style={{ width: '100%', height: '100%', backgroundColor: 'red' }}></div>
      </GridLayoutCell>,
    ],
  },
};
export const SpanHalfRow: Story = {
  args: {
    templateColumns: 6,
    children: [
      <GridLayoutCell columnSpan={3}>
        <div style={{ width: '100%', height: '100%', backgroundColor: 'green' }}></div>
      </GridLayoutCell>,
      <GridLayoutCell columnSpan={3}>
        <div style={{ width: '100%', height: '100%', backgroundColor: 'grey' }}></div>
      </GridLayoutCell>,
    ],
  },
};
export const SpanOneThirdRow: Story = {
  args: {
    templateColumns: 6,
    children: [
      <GridLayoutCell columnSpan={2} alignItems="center">
        <div style={{ width: '100%', height: '100%', backgroundColor: 'green' }}></div>
      </GridLayoutCell>,
      <GridLayoutCell columnSpan={2}>
        <div style={{ width: '100%', height: '100%', backgroundColor: 'grey' }}></div>
      </GridLayoutCell>,
      <GridLayoutCell columnSpan={2}>
        <div style={{ width: '100%', height: '100%', backgroundColor: 'red' }}></div>
      </GridLayoutCell>,
    ],
  },
};
