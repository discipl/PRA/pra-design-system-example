import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/GridLayout/GridLayout.scss';

interface GridLayoutSpecificProps {
  templateColumns?: number;
}

export interface GridLayoutdProps extends HTMLAttributes<HTMLDivElement>, GridLayoutSpecificProps {}

export const GridLayout: React.ForwardRefExoticComponent<
  GridLayoutdProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { children, templateColumns = 6, ...otherProps }: PropsWithChildren<GridLayoutdProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div
        ref={ref}
        className="pra-grid-layout"
        style={
          {
            '--pra-grid-layout-template-columns': `${templateColumns}`,
          } as React.CSSProperties
        }
        {...otherProps}
      >
        {children}
      </div>
    );
  },
);

GridLayout.displayName = 'GridLayout';
