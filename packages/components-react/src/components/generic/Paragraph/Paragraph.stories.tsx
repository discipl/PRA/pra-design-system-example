import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Paragraph/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Paragraph } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Paragraph> = {
  component: Paragraph,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Paragraph',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'paragraph',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Paragraph>;

export const Default: Story = {
  args: {
    children: [
      'Wij stellen U eerst een aantal vragen om er zeker van te zijn dat U beschikt over de juiste informatie voordat U het aanvraagformulier gaat invullen. Bij het invullen van de formulier heeft u de volgende bijlagen nodig:',
    ],
  },
};

export const Small: Story = {
  args: {
    ...Default.args,
    small: true,
  },
};
