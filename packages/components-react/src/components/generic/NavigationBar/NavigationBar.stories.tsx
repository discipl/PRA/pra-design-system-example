import shape from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/Shape';
import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import settings from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Settings';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/NavigationBar/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { NavigationBar } from './index';
import { NavigationBarItem } from '../NavigationBarItem';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof NavigationBar> = {
  component: NavigationBar,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/NavigationBar',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'navigationbar',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof NavigationBar>;

export const Default: Story = {
  args: {
    children: [
      <NavigationBarItem
        selected={true}
        indicator={false}
        text="Dashboard"
        icon={layout_grid({})}
        onClickHandler={() => {}}
      />,
      <NavigationBarItem
        selected={false}
        indicator={false}
        text="Mijn PRA"
        icon={shape({})}
        onClickHandler={() => {}}
      />,
      <NavigationBarItem
        selected={false}
        indicator={false}
        text="Instellingen"
        icon={settings({})}
        onClickHandler={() => {}}
      />,
    ],
  },
};
