import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/NavigationBar/NavigationBar.scss';
import React from 'react';

interface NavigationBarSpecificProps {}
export interface NavigationBarProps extends HTMLAttributes<HTMLInputElement>, NavigationBarSpecificProps {}

export const NavigationBar: React.ForwardRefExoticComponent<
  NavigationBarProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ children }: PropsWithChildren<NavigationBarProps>, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <div ref={ref} className="pra-navigation-bar">
      {children && <ul>{children}</ul>}
    </div>
  );
});

NavigationBar.displayName = 'NavigationBar';
