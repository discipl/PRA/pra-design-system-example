import ReadMe from '@persoonlijke-regelingen-assistent/components-css/RequiredIndicator/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { RequiredIndicator } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof RequiredIndicator> = {
  component: RequiredIndicator,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/RequiredIndicator',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'requiredindicator',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof RequiredIndicator>;

export const Required: Story = {
  args: {
    required: true,
  },
};

export const NotRequired: Story = {
  args: {
    required: false,
  },
};
