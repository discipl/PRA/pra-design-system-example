import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ListCalculation/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ListCalculation } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof ListCalculation> = {
  component: ListCalculation,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/ListCalculation',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'listcalculation',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ListCalculation>;

export const Default: Story = {
  args: {
    title: 'Inkomen',
    key: 'key1',
    initCollapsed: false,
    data: [
      { label: 'Pensioenuitkeringen', amount: 60795 },
      { isSubpart: true, label: 'PensioenFonds 1', amount: 50000 },
      { isSubpart: true, label: 'PensioenFonds 2', amount: 10795 },
      { label: 'AOW', amount: 18031 },
      { label: 'Overige inkomen' },
      { isTotal: true, label: 'Totaal bruto inkomen op jaarbasis', amount: 78826 },
    ],
  },
};
