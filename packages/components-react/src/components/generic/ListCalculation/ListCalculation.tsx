import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/ListCalculation/ListCalculation.scss';
import { Dropdown } from '../Dropdown';

interface RowData {
  label: string;
  amount?: number;
  isSubpart?: boolean;
  isTotal?: boolean;
}
interface ListCalculationSpecificProps {
  title?: string;
  key?: string;
  initCollapsed?: boolean;
  data?: RowData[];
  calculationSymbol: string;
}
export interface ListCalculationProps extends ListCalculationSpecificProps {}

export const ListCalculation: React.ForwardRefExoticComponent<
  ListCalculationProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      data = [],
      title = '',
      key = '',
      initCollapsed = false,
      calculationSymbol,
    }: PropsWithChildren<ListCalculationProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <Dropdown
        ref={ref}
        key={key}
        title={title}
        variant="Default"
        initCollapsed={initCollapsed}
        className="pra-list-calculation"
      >
        <table className="pra-list-calculation-table">
          {data.map((rowData: RowData, index) => {
            return (
              <tr
                className={`pra-list-calculation-row ${rowData.isSubpart ? 'pra-list-calculation-row-subpart' : ''} `}
              >
                <td>{rowData.label}</td>
                <td>{rowData.amount ? `€ ${rowData.amount}` : 'onbekend'}</td>
                {index === data.length - 1 && (
                  <span className="pra-list-calculation-calculation-symbol">{calculationSymbol}</span>
                )}
              </tr>
            );
          })}
        </table>
      </Dropdown>
    );
  },
);

ListCalculation.displayName = 'ListCalculation';
