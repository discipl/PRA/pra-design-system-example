import ReadMe from '@persoonlijke-regelingen-assistent/components-css/BackLink/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { BackLink } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof BackLink> = {
  component: BackLink,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/BackLink',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'backlink',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof BackLink>;

export const Default: Story = {
  args: {
    onClick: () => {
      console.log('click');
    },
    children: ['terug'],
  },
};
