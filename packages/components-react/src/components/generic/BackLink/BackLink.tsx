import chevron_left from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronLeft';
import React, { AnchorHTMLAttributes, ForwardedRef, forwardRef } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/BackLink/BackLink.scss';

interface BackLinkSpecificProps {}
export interface BackLinkProps extends AnchorHTMLAttributes<HTMLAnchorElement>, BackLinkSpecificProps {}

export const BackLink: React.ForwardRefExoticComponent<BackLinkProps & React.RefAttributes<HTMLAnchorElement>> =
  forwardRef(({ ...props }: BackLinkProps, ref: ForwardedRef<HTMLAnchorElement>) => {
    const { children } = props;
    return (
      <a className="pra-back-link" ref={ref} role="link" {...props}>
        {chevron_left({})}
        {children}
      </a>
    );
  });

BackLink.displayName = 'BackLink';
