import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import layout_grid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/BreadcrumbNav/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { BreadcrumbNav } from './index';
import { BreadcrumbNavLink } from '../BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../BreadcrumbNavSeparator';
import { Icon } from '../Icon';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof BreadcrumbNav> = {
  component: BreadcrumbNav,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Breadcrumb',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'breadcrumb',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};
export default meta;
type Story = StoryObj<typeof BreadcrumbNav>;

export const Default: Story = {
  args: {
    children: [
      <BreadcrumbNavLink current={false} href="#" index={0}>
        <Icon>{layout_grid({})}</Icon>
      </BreadcrumbNavLink>,
      <BreadcrumbNavSeparator>
        <Icon>{ChevronRight({})}</Icon>
      </BreadcrumbNavSeparator>,
      <BreadcrumbNavLink current={false} href="#" index={1}>
        Levensgebeurtenis
      </BreadcrumbNavLink>,
      <BreadcrumbNavSeparator>
        <Icon>{ChevronRight({})}</Icon>
      </BreadcrumbNavSeparator>,
      <BreadcrumbNavLink current={true} href="#" index={2}>
        DigiD aanvragen
      </BreadcrumbNavLink>,
    ],
  },
};
