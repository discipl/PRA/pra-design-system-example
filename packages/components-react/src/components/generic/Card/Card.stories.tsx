import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Card/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Card } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof Card> = {
  component: Card,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Card',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'card',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Card>;

export const Default: Story = {
  args: {
    imgAlt: 'Birthday Party',
    imgSrc: birthday_party,
    linkHref: '#',
    linkLabel: 'Bekijk levensgebeurtenis',
    description:
      'Je bent vandaag of onlangs 18 jaar geworden. Er zijn een aantal belangrijke zaken die je moet regelen.',
    title: '18 jaar worden: Wat moet ik regelen?',
  },
};
