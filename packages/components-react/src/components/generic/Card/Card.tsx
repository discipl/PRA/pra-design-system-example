import React, { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { Heading } from '../Heading';
import { Image } from '../Image';
import { Link } from '../Link';
import { Paragraph } from '../Paragraph';
import '@persoonlijke-regelingen-assistent/components-css/Card/Card.scss';

interface CardSpecificProps {
  title?: string;
  description?: string;
  imgSrc?: string;
  imgAlt?: string;
  linkHref?: string;
  linkLabel?: string;
}

export interface CardProps extends CardSpecificProps {}

export const Card: React.ForwardRefExoticComponent<
  CardProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      title = '',
      description = '',
      imgSrc = '',
      imgAlt = '',
      linkLabel = '',
      linkHref = '',
    }: PropsWithChildren<CardProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className="pra-card">
        <Image className="utrecht-img--scale-down" src={imgSrc} alt={imgAlt} />
        <Heading level={2}>{title}</Heading>
        <Paragraph>{description}</Paragraph>
        <Link href={linkHref}>{linkLabel}</Link>
      </div>
    );
  },
);

Card.displayName = 'Card';
