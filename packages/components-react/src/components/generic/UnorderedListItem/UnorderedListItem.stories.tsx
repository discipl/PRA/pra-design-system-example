import chevron_right from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/UnorderedListItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { UnorderedListItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof UnorderedListItem> = {
  component: UnorderedListItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/UnorderedListItem',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'unorderedlistitem',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof UnorderedListItem>;

export const WithMarkerSVG: Story = {
  args: {
    markerContent: chevron_right({ fill: '#FF9213' }),
    children: ['Goed voorbereid op je pensioen'],
  },
};

export const WithMarkerContent: Story = {
  args: {
    markerContent: '?',
    children: ['Goed voorbereid op je pensioen'],
  },
};
