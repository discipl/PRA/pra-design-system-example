import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Heading/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Heading } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Heading> = {
  component: Heading,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Heading',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'heading',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Heading>;

export const Level1: Story = {
  args: {
    level: 1,
    children: 'Heading',
  },
};

export const Level2: Story = {
  args: {
    level: 2,
    children: 'Heading',
  },
};

export const Level3: Story = {
  args: {
    level: 3,
    children: 'Heading',
  },
};

export const Level4: Story = {
  args: {
    level: 4,
    children: 'Heading',
  },
};

export const Level5: Story = {
  args: {
    level: 5,
    children: 'Heading',
  },
};
