import { Children, ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/Carousel/Carousel.scss';

interface carouselSpecificProps {
  showNavigation?: boolean;
  slideWidth?: string;
  slideGap?: string;
  scrollSnap?: 'proximity' | 'mandatory';
}

export interface carouselProps extends carouselSpecificProps {}

export const Carousel: React.ForwardRefExoticComponent<
  carouselProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      children = [],
      showNavigation = false,
      slideWidth = '250px',
      slideGap = '20px',
      scrollSnap = 'proximity',
    }: PropsWithChildren<carouselProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    let navigationDots: JSX.Element[] | null | undefined = [];
    let slides: JSX.Element[] | null | undefined = [];

    if (children) {
      slides = Children.map(children, (child, index) => (
        <div className="pra-carousel-slide" id={`pra-carousel-slide-${index}`}>
          {child}
        </div>
      ));
      if (showNavigation) {
        navigationDots = Children.map(children, (_child, index) => (
          <a className="pra-carousel-navigation-rect" href={`#pra-carousel-slide-${index}`} />
        ));
      }
    }

    return (
      <div
        ref={ref}
        className="pra-carousel"
        style={
          {
            '--pra-carousel-slide-width': `${slideWidth}`,
            '--pra-carousel-slide-gap': `${slideGap}`,
            '--pra-carousel-scroll-snap': `${scrollSnap}`,
          } as React.CSSProperties
        }
      >
        <div className="pra-carousel-slides">{slides}</div>
        {showNavigation && <div className="pra-carousel-navigation">{navigationDots}</div>}
      </div>
    );
  },
);

Carousel.displayName = 'Carousel';
