import '@persoonlijke-regelingen-assistent/components-css/MenuItem/MenuItem.scss';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';

interface MenuItemSpecificProps {
  icon?: JSX.Element;
  imageSrc?: string;
  label?: string;
  mode?: 'horizontal' | 'vertical';
  size?: 'small' | 'medium' | 'large' | 'extra-large';
}

export interface MenuItemProps extends HTMLAttributes<HTMLDivElement>, MenuItemSpecificProps {}

export const MenuItem: React.ForwardRefExoticComponent<
  MenuItemProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { label, icon, imageSrc, mode = 'vertical', size = 'medium', ...otherProps }: PropsWithChildren<MenuItemProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div
        ref={ref}
        className={`pra-menu-item ${mode ? `pra-menu-item--${mode}` : ''} ${size ? `pra-menu-item--${size}` : ''}`}
        {...otherProps}
      >
        {imageSrc ? (
          <img className=" pra-menu-item-image" src={imageSrc} />
        ) : (
          icon && <span className=" pra-menu-item-icon">{icon}</span>
        )}
        <label className="pra-menu-item-label">{label}</label>
      </div>
    );
  },
);

MenuItem.displayName = 'MenuItem';
