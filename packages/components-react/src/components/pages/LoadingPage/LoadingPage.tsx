import { ForwardedRef, forwardRef, HTMLAttributes } from 'react';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Page } from '../../specific/Page';
import { Spinner } from '../../specific/Spinner';

interface LoadingPageSpecificProps {}
export interface LoadingPageProps extends HTMLAttributes<HTMLDivElement>, LoadingPageSpecificProps {}

export const LoadingPage: React.ForwardRefExoticComponent<LoadingPageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(({ ...props }: LoadingPageProps, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <Page alternateTheme={false} ref={ref} {...props} style={{ backgroundColor: '#ffffff' }}>
        <GridLayout>
          <GridLayoutCell columnSpan={6} style={{ minBlockSize: '100%' }}>
            <Spinner></Spinner>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  });

LoadingPage.displayName = 'LoadingPage';
