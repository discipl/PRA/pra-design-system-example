import type { Meta, StoryObj } from '@storybook/react';
import { NotificationsOverviewPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof NotificationsOverviewPage> = {
  component: NotificationsOverviewPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/NotificationsOverviewPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof NotificationsOverviewPage>;

export const Default: Story = {
  args: {},
};
