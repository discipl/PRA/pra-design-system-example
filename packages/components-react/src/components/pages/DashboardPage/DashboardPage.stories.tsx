import type { Meta, StoryObj } from '@storybook/react';
import { DashboardPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof DashboardPage> = {
  component: DashboardPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/DashboardPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof DashboardPage>;

export const Default: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    greeting: 'Goedemorgen',
    speechBulbContent: 'Hulp nodig?',
    onSearchBarEnterKey: () => {},
    themesList: [
      {
        icon: 'Hoogbouw',
        label: 'Bouwen en wonen',
      },
      {
        icon: 'Toeslag',
        label: 'Belastingen, uitkeringen en toeslagen',
      },
      {
        icon: 'EconomieWerkInkomen',
        label: 'Werk',
      },
      {
        icon: 'Inkomen',
        label: 'Economie',
      },
      {
        icon: 'PaspoortIdkaartGecombineerd',
        label: 'Migratie en reizen',
      },
      {
        icon: 'KindEnFamilie',
        label: 'Familie, zorg en gezondheid',
      },
      {
        icon: 'Politiek',
        label: 'Overheid en democratie',
      },
      {
        icon: 'NatuurLandschap',
        label: 'Klimaat, milieu en natuur',
      },
      {
        icon: 'Ondernemen',
        label: 'Internationale samenwerking',
      },
      {
        icon: 'Auto',
        label: 'Verkeer en vervoer',
      },
      {
        icon: 'Kennis',
        label: 'Onderwijs',
      },
      {
        icon: 'BezwaarEnBeroep',
        label: 'Recht, veiligheid en defensie',
      },
    ],
    lifeEventsList: [
      {
        name: 'Woning kopen: wat moet ik regelen?',
        id: 58001,
        description: 'Woning kopen: wat moet ik regelen?',
      },
      {
        name: '18 jaar worden: wat moet ik regelen?',
        id: 57996,
        description:
          'Als 18 jaar wordt zijn er een aantal belangrijke zaken die geregeld moeten worden. Dit overzicht helpt je hierbij.\n',
      },
      {
        name: 'Woning huren: wat moet ik regelen?',
        id: 58002,
        description:
          'U gaat een woning huren. Dan krijgt u onder meer te maken met het inschrijven bij een woningcorporatie, het huurcontract en mogelijk met huurtoeslag.',
      },
      {
        name: 'Werkloos worden: wat moet ik regelen?',
        id: 57994,
        description: 'Werkloos worden: wat moet ik regelen?',
      },
      {
        name: 'Met pensioen: wat moet ik regelen?',
        id: 58029,
        description: 'Met pensioen: wat moet ik regelen?',
      },
    ],
    baseURL: '/?path=/docs/pra-ds-pages-dashboardpage--docs#',
    onThemeClick: () => {},
  },
};
