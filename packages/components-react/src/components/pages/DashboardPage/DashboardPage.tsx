import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import * as iconsImport from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak';
import birthdayPartyImg from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { ForwardedRef, forwardRef, HTMLAttributes, ReactNode, type SVGProps, useState } from 'react';
import { Card } from '../../generic/Card';
import { Carousel } from '../../generic/Carousel';
import { ContentArea } from '../../generic/ContentArea';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Link } from '../../generic/Link';
import { MenuItem } from '../../generic/MenuItem';
import { Paragraph } from '../../generic/Paragraph';
import { Textbox } from '../../generic/Textbox';
import { UnorderedList } from '../../generic/UnorderedList';
import { UnorderedListItem } from '../../generic/UnorderedListItem';
import { Page } from '../../specific/Page';

// eslint-disable-next-line no-unused-vars
const icons = iconsImport as Record<string, (props: SVGProps<SVGSVGElement>) => JSX.Element>;
// eslint-disable-next-line no-unused-vars
const nameToIconMapper = (name: string): ((props: SVGProps<SVGSVGElement>) => JSX.Element) | undefined => {
  return icons[name];
};

interface DashboardPageSpecificProps {
  navigationBar?: ReactNode;
  greeting: string;
  onSearchBarEnterKey: Function;
  themesList: ThemesListItem[];
  lifeEventsList: LifeEventsListItem[];
  baseURL?: string;
  onThemeClick: Function;
}
export interface DashboardPageProps extends HTMLAttributes<HTMLDivElement>, DashboardPageSpecificProps {}
interface LifeEventsListItem {
  name: string;
  id: number;
  description: string;
}

interface ThemesListItem {
  label: string;
  icon: string;
}

export const DashboardPage: React.ForwardRefExoticComponent<DashboardPageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(
    (
      {
        navigationBar,
        greeting,
        onSearchBarEnterKey,
        themesList,
        lifeEventsList,
        baseURL = '/',
        onThemeClick,
        ...props
      }: DashboardPageProps,
      ref: ForwardedRef<HTMLDivElement>,
    ) => {
      const [searchQuery, setSearchQuery] = useState<string>('');
      const onSearchBarKeyDown = (event: React.KeyboardEvent): void => {
        if (event.key === 'Enter') {
          onSearchBarEnterKey(searchQuery);
        }
      };
      return (
        <Page navigationBar={navigationBar} {...props} ref={ref}>
          <GridLayout templateColumns={6}>
            <GridLayoutCell columnSpan={6}>
              <Heading level={1}>{greeting}</Heading>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6}>
              <Textbox
                placeholder="Waar bent u naar op zoek?"
                onChange={(e) => {
                  setSearchQuery(e.target.value);
                }}
                onKeyDown={(e) => {
                  onSearchBarKeyDown(e);
                }}
                iconStart={Search({})}
                invalid={false}
              />
            </GridLayoutCell>
            <GridLayoutCell columnSpan={3}>
              <Heading level={3}>{`Thema's`}</Heading>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={3} alignItems="end">
              <Paragraph>
                <Link href={`${baseURL}themes-overview`}>Bekijk pagina</Link>
              </Paragraph>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6}>
              <Carousel showNavigation={false} slideWidth={'96px'} slideGap={'8px'}>
                {themesList.map(({ label, icon }: ThemesListItem) => {
                  const _icon = nameToIconMapper(icon);
                  return (
                    <MenuItem
                      key={label}
                      icon={_icon ? _icon({}) : undefined}
                      label={label}
                      onClick={() => onThemeClick({ label, icon })}
                    />
                  );
                })}
              </Carousel>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={3}>
              <Heading level={3}>Levensgebeurtenissen</Heading>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={3} alignItems="end">
              <Link href={`${baseURL}life-events`}>Bekijk pagina</Link>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6}>
              <Carousel showNavigation={false} slideWidth={'250px'} slideGap={'20px'}>
                {lifeEventsList.map((lifeEventsListItem: LifeEventsListItem) => {
                  const { name, id, description } = lifeEventsListItem;
                  return (
                    <Card
                      key={name}
                      imgAlt="Birthday Party"
                      imgSrc={birthdayPartyImg}
                      linkHref={`${baseURL}life-events/${id}`}
                      linkLabel="Bekijk levensgebeurtenis"
                      description={description}
                      title={name}
                    />
                  );
                })}
              </Carousel>
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6}>
              <ContentArea>
                <Heading level={3}>Nieuws & Tips</Heading>
                <UnorderedList>
                  <UnorderedListItem markerContent={ChevronRight({ fill: '#FF9213' })}>
                    Goed voorbereid op je pensioen.
                  </UnorderedListItem>
                  <UnorderedListItem markerContent={ChevronRight({ fill: '#FF9213' })}>
                    Kinderopvangtoeslag in 2024 extra verhoogd.
                  </UnorderedListItem>
                  <UnorderedListItem markerContent={ChevronRight({ fill: '#FF9213' })}>
                    Geldt het prijsplafond voor energie ook in 2024?
                  </UnorderedListItem>
                </UnorderedList>
              </ContentArea>
            </GridLayoutCell>
          </GridLayout>
        </Page>
      );
    },
  );

DashboardPage.displayName = 'DashboardPage';
