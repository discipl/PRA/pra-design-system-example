import WelcomeVector1 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector1';
import WelcomeVector2 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector2';
import WelcomeVector3 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector3';
import user_login from '@persoonlijke-regelingen-assistent/assets/dist/images/user-login.png';
import user_todo from '@persoonlijke-regelingen-assistent/assets/dist/images/user-todo.png';
import user_welcome from '@persoonlijke-regelingen-assistent/assets/dist/images/user-welcome.png';
import { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler } from 'react';
import { Button } from '../../generic/Button';
import { Carousel } from '../../generic/Carousel';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { InfoSlide } from '../../generic/InfoSlide';
import { IntroductionBackground } from '../../specific/IntroductionBackground';
import { Page } from '../../specific/Page';

interface InfoPageSpecificProps {
  nextButtonAction: MouseEventHandler;
}
export interface InfoPageProps extends HTMLAttributes<HTMLDivElement>, InfoPageSpecificProps {}

export const InfoPage: React.ForwardRefExoticComponent<InfoPageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(({ nextButtonAction, ...props }: InfoPageProps, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <Page alternateTheme={true} backgroundFC={(props) => <IntroductionBackground {...props} />} ref={ref} {...props}>
        <GridLayout templateColumns={6}>
          <GridLayoutCell columnSpan={6}>
            <Heading level={1} style={{ color: 'var(--pra-color-primary-600)' }}>
              Wat kun je doen met jouw Persoonlijke Regelingen Assistent?
            </Heading>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6} alignItems="center" paddingInline={0}>
            <Carousel showNavigation={true} slideWidth={'100%'} slideGap="20px" scrollSnap="mandatory">
              <InfoSlide
                backgroundVector={WelcomeVector1}
                imageSrc={user_welcome}
                imageAlt="user-welcome"
                information="Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae."
              />
              <InfoSlide
                backgroundVector={WelcomeVector2}
                imageSrc={user_todo}
                imageAlt="user-todo"
                information="Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae."
              />
              <InfoSlide
                backgroundVector={WelcomeVector3}
                imageSrc={user_login}
                imageAlt="user-login"
                information="Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae."
              />
            </Carousel>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6} alignItems="center">
            <Button appearance="secondary-action-button" onClick={(e) => nextButtonAction(e)}>
              Volgende
            </Button>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  });

InfoPage.displayName = 'InfoPage';
