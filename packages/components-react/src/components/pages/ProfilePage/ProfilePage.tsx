// import user from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/User';
import { FormEvent, ForwardedRef, forwardRef, HTMLAttributes, ReactNode, useState } from 'react';
import { Button } from '../../generic/Button';
import { FormLabel } from '../../generic/FormLabel';
import { FormSelectionButtons } from '../../generic/FormSelectionButtons';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
// import { Icon } from '../../generic/Icon';
// import { Image } from '../../generic/Image';
// import { Link } from '../../generic/Link';
import { Paragraph } from '../../generic/Paragraph';
import { Textbox } from '../../generic/Textbox';
import { Page } from '../../specific/Page';

interface Profile {
  firstname?: string;
  birthdate?: string;
  postalcode?: string;
  tvdistinction?: 'T' | 'V';
  avatar?: string;
}

interface ProfilePageSpecificProps {
  // eslint-disable-next-line no-unused-vars
  onProfileDataChange: (profileKey: keyof Profile, newValue: string | boolean) => void;
  avatarChangeAction: () => void;
  profile: Profile;
  initialEditMode?: boolean;
  navigationBar?: ReactNode;
}
export interface ProfilePageProps extends HTMLAttributes<HTMLDivElement>, ProfilePageSpecificProps {}

export const ProfilePage: React.ForwardRefExoticComponent<ProfilePageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(
    (
      {
        onProfileDataChange,
        // avatarChangeAction,
        profile,
        navigationBar,
        initialEditMode = false,
        ...props
      }: ProfilePageProps,
      ref: ForwardedRef<HTMLDivElement>,
    ) => {
      const [_editMode, _setEditMode] = useState<boolean>(initialEditMode);
      return (
        <Page alternateTheme={false} ref={ref} navigationBar={navigationBar} {...props}>
          <GridLayout templateColumns={6}>
            <GridLayoutCell columnSpan={6}>
              <Heading level={2}>Profiel</Heading>
              <Paragraph>Maak een profiel aan om proactieve meldingen te ontvangen.</Paragraph>
            </GridLayoutCell>
            {/* <GridLayoutCell columnSpan={6} alignItems="center" justifyContent="center" paddingInline={0}>
              {profile?.avatar ? (
                <Image style={{ height: '128px', width: '128px', 'border-radius': '50%' }} src={profile?.avatar} />
              ) : (
                <Icon style={{ height: '128px', width: '128px' }}>{user({})}</Icon>
              )}
              <Link
                href=""
                onClick={(e: MouseEvent) => {
                  e.preventDefault();
                  avatarChangeAction();
                }}
              >
                Wijzig afbeelding
              </Link>
            </GridLayoutCell> */}
            <GridLayoutCell columnSpan={6}>
              <FormLabel>
                <Paragraph>
                  <b>Naam</b>
                </Paragraph>
              </FormLabel>
              {_editMode ? (
                <Textbox
                  placeholder="Type hier..."
                  invalid={false}
                  value={profile?.firstname}
                  onChange={(e: FormEvent<HTMLInputElement>) =>
                    onProfileDataChange('firstname', e?.currentTarget?.value)
                  }
                />
              ) : (
                profile?.firstname ?? '-'
              )}
              <FormLabel>
                <Paragraph>
                  <b>Geboortedatum</b>
                </Paragraph>
              </FormLabel>
              {_editMode ? (
                <Textbox
                  type="date"
                  placeholder=""
                  invalid={false}
                  value={profile?.birthdate}
                  onChange={(e: FormEvent<HTMLInputElement>) =>
                    onProfileDataChange('birthdate', e?.currentTarget?.value)
                  }
                />
              ) : (
                profile?.birthdate ?? '-'
              )}
              <FormLabel>
                <Paragraph>
                  <b>Postcode</b>
                </Paragraph>
              </FormLabel>
              {_editMode ? (
                <Textbox
                  placeholder="Type hier..."
                  invalid={false}
                  value={profile?.postalcode}
                  onChange={(e: FormEvent<HTMLInputElement>) =>
                    onProfileDataChange('postalcode', e?.currentTarget?.value)
                  }
                />
              ) : (
                profile?.postalcode ?? '-'
              )}
              <FormLabel>
                <Paragraph>
                  <b>Aanspreekvorm</b>
                </Paragraph>
              </FormLabel>
              {_editMode ? (
                <FormSelectionButtons
                  initialValue={
                    profile?.tvdistinction === 'V' ? 'U' : profile?.tvdistinction === 'T' ? 'je/jij' : undefined
                  }
                  options={[
                    { label: 'U', value: 'V' },
                    { label: 'Je/Jij', value: 'T' },
                  ]}
                  onValueChange={(value) => {
                    if (value) {
                      onProfileDataChange('tvdistinction', value);
                    }
                  }}
                />
              ) : profile?.tvdistinction === 'V' ? (
                'U'
              ) : profile?.tvdistinction === 'T' ? (
                'Je/Jij'
              ) : (
                '-'
              )}
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6} alignItems="center" justifyContent="center" paddingInline={0}>
              <Button onClick={() => _setEditMode(!_editMode)}>Profiel Bewerken</Button>
            </GridLayoutCell>
          </GridLayout>
        </Page>
      );
    },
  );

ProfilePage.displayName = 'ProfilePage';
