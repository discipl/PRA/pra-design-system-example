import Bookmark from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Bookmark';
import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { ForwardedRef, forwardRef, HTMLAttributes, ReactNode } from 'react';
import { ButtonBadge } from '../../generic/ButtonBadge';
import { ButtonGroup } from '../../generic/ButtonGroup';
import { Carousel } from '../../generic/Carousel';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { MenuItem } from '../../generic/MenuItem';
import { Paragraph } from '../../generic/Paragraph';
import { SavedItem } from '../../generic/SavedItem';
import { UnorderedList } from '../../generic/UnorderedList';
import { UnorderedListItem } from '../../generic/UnorderedListItem';
import { Page } from '../../specific/Page';

export interface SavedItemBaseProps {
  id: number;
}
export interface Regulation extends SavedItemBaseProps {
  type: 'regulation';
  title?: string;
  message?: string;
}
export interface LifeEvent extends SavedItemBaseProps {
  type: 'lifeEvent';
  label?: string;
  imageSrc?: string;
}
export interface NewsItem extends SavedItemBaseProps {
  type: 'news';
  title?: string;
  message?: string;
  notificationMessage?: string;
}
interface SavedItemsPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  // eslint-disable-next-line no-unused-vars
  lifeEventClickAction: (id: number) => void;
  // eslint-disable-next-line no-unused-vars
  regulationClickAction: (id: number) => void;
  profiles?: { name: string; pressed: boolean }[];
  savedRegulations?: Regulation[];
  savedLifeEvents?: LifeEvent[];
  savedNews?: NewsItem[];
}
export interface SavedItemsPageProps extends HTMLAttributes<HTMLDivElement>, SavedItemsPageSpecificProps {}

export const SavedItemsPage: React.ForwardRefExoticComponent<
  SavedItemsPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    {
      navigationBar,
      profiles = [],
      savedRegulations = [],
      savedLifeEvents = [],
      savedNews = [],
      lifeEventClickAction,
      regulationClickAction,
      ...props
    }: SavedItemsPageProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <Page navigationBar={navigationBar} {...props} ref={ref}>
        <GridLayout templateColumns={6}>
          <GridLayoutCell columnSpan={6}>
            <Heading level={2}>Opgeslagen Items</Heading>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6} justifyContent="start">
            <ButtonGroup>
              {profiles.map(({ name, pressed }) => {
                return (
                  <ButtonBadge pressed={pressed} appearance={'primary'}>
                    {name}
                  </ButtonBadge>
                );
              })}
            </ButtonGroup>
          </GridLayoutCell>
          {savedLifeEvents.length === 0 && savedRegulations.length === 0 && savedNews.length === 0 && (
            <GridLayoutCell columnSpan={6} justifyContent="start">
              <Heading level={2}>Je hebt nog geen regelingen opgelsagen.</Heading>
              <Paragraph>
                Je kan regelingen die je interessant vindt of wilt bewaren voor later opslaan door op het Bookmark icoon
                te klikken naast de regelingen. Hieronder vindt je enkele veel bekeken regelingen:
              </Paragraph>
              <UnorderedList>
                <UnorderedListItem markerContent={ChevronRight({})}> Kinderbijslag </UnorderedListItem>
                <UnorderedListItem markerContent={ChevronRight({})}>
                  <>Subsidie voor isolatiemaatregelen en (hybride) waterpompen</>
                </UnorderedListItem>
                <UnorderedListItem markerContent={ChevronRight({})}>Pensioenregeling</UnorderedListItem>
              </UnorderedList>
            </GridLayoutCell>
          )}
          {savedLifeEvents.length > 0 && (
            <GridLayoutCell columnSpan={6}>
              <Heading level={3}>Opgeslagen gebeurtenissen</Heading>
              <div style={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
                <Icon style={{ height: '24px', width: '24px' }}>
                  {Bookmark({
                    fill: 'var(--pra-color-primary-300)',
                    style: { display: 'flex', alignItems: 'center', justifyContent: 'center', height: '20px' },
                  })}
                </Icon>
                <Paragraph style={{ color: 'var(--pra-color-primary-300)' }}>{savedLifeEvents.length}</Paragraph>
              </div>
              <Carousel showNavigation={false} slideWidth="96px" slideGap="8px">
                {savedLifeEvents.map(({ imageSrc, label, id }) => {
                  return (
                    <MenuItem imageSrc={imageSrc} label={label} size="large" onClick={() => lifeEventClickAction(id)} />
                  );
                })}
              </Carousel>
            </GridLayoutCell>
          )}
          {savedRegulations.length > 0 && (
            <GridLayoutCell columnSpan={6}>
              <Heading level={3}>Opgeslagen regelingen</Heading>
              <div style={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
                <Icon style={{ height: '24px', width: '24px' }}>
                  {Bookmark({
                    fill: 'var(--pra-color-primary-300)',
                    style: { display: 'flex', alignItems: 'center', justifyContent: 'center', height: '20px' },
                  })}
                </Icon>
                <Paragraph style={{ color: 'var(--pra-color-primary-300)' }}>{savedRegulations.length}</Paragraph>
              </div>
              <div style={{ display: 'flex', width: '100%', flexDirection: 'column', gap: '16px' }}>
                {savedRegulations.map(({ title, message, id }) => {
                  return (
                    <SavedItem
                      title={title}
                      message={message}
                      savedItemClickAction={() => regulationClickAction(id)}
                    ></SavedItem>
                  );
                })}
              </div>
            </GridLayoutCell>
          )}
          {savedNews.length > 0 && (
            <GridLayoutCell columnSpan={6}>
              <Heading level={3}>Opgeslagen nieuws & tips</Heading>
              <div style={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
                <Icon style={{ height: '24px', width: '24px' }}>
                  {Bookmark({
                    fill: 'var(--pra-color-primary-300)',
                    style: { display: 'flex', alignItems: 'center', justifyContent: 'center', height: '20px' },
                  })}
                </Icon>
                <Paragraph style={{ color: 'var(--pra-color-primary-300)' }}>{savedNews.length}</Paragraph>
              </div>
              {savedNews.map(({ title, message, notificationMessage }) => {
                return (
                  <SavedItem
                    title={title}
                    message={message}
                    notificationMessage={notificationMessage}
                    savedItemClickAction={() => {}}
                  ></SavedItem>
                );
              })}
            </GridLayoutCell>
          )}
        </GridLayout>
      </Page>
    );
  },
);

SavedItemsPage.displayName = 'SavedItemsPage';
