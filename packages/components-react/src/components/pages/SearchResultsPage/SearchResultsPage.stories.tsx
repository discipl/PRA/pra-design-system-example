import type { Meta, StoryObj } from '@storybook/react';
import { SearchResultsPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SearchResultsPage> = {
  component: SearchResultsPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/SearchResultsPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof SearchResultsPage>;

export const Default: Story = {
  args: {
    error: false,
    numberOfResults: 5,
  },
};
export const NoResults: Story = {
  args: {
    ...Default.args,
    numberOfResults: 0,
  },
};
export const Error: Story = {
  args: {
    ...Default.args,
    error: true,
  },
};
