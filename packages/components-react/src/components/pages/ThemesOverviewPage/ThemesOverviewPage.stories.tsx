import type { Meta, StoryObj } from '@storybook/react';
import { ThemesOverviewPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof ThemesOverviewPage> = {
  component: ThemesOverviewPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/ThemesOverviewPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof ThemesOverviewPage>;

export const Default: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-themesoverviewpage--docs#',
    themesList: [
      {
        icon: 'Hoogbouw',
        label: 'Bouwen en wonen',
        onClick: () => {},
      },
      {
        icon: 'Toeslag',
        label: 'Belastingen, uitkeringen en toeslagen',
        onClick: () => {},
      },
      {
        icon: 'EconomieWerkInkomen',
        label: 'Werk',
        onClick: () => {},
      },
      {
        icon: 'Inkomen',
        label: 'Economie',
        onClick: () => {},
      },
      {
        icon: 'PaspoortIdkaartGecombineerd',
        label: 'Migratie en reizen',
        onClick: () => {},
      },
      {
        icon: 'KindEnFamilie',
        label: 'Familie, zorg en gezondheid',
        onClick: () => {},
      },
      {
        icon: 'Politiek',
        label: 'Overheid en democratie',
        onClick: () => {},
      },
      {
        icon: 'NatuurLandschap',
        label: 'Klimaat, milieu en natuur',
        onClick: () => {},
      },
      {
        icon: 'Ondernemen',
        label: 'Internationale samenwerking',
        onClick: () => {},
      },
      {
        icon: 'Auto',
        label: 'Verkeer en vervoer',
        onClick: () => {},
      },
      {
        icon: 'Kennis',
        label: 'Onderwijs',
        onClick: () => {},
      },
      {
        icon: 'BezwaarEnBeroep',
        label: 'Recht, veiligheid en defensie',
        onClick: () => {},
      },
    ],
  },
};
