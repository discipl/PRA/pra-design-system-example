import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode } from 'react';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { MenuItem } from '../../generic/MenuItem';
import { Paragraph } from '../../generic/Paragraph';
import { Page } from '../../specific/Page';
interface LifeEventsListItem {
  name: string;
  id: number;
  description: string;
  onClick: MouseEventHandler<HTMLDivElement>;
}

interface LifeEventsOverviewPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  lifeEventsList?: LifeEventsListItem[];
}
export interface LifeEventsOverviewPageProps
  extends HTMLAttributes<HTMLDivElement>,
    LifeEventsOverviewPageSpecificProps {}

export const LifeEventsOverviewPage: React.ForwardRefExoticComponent<
  LifeEventsOverviewPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { navigationBar, baseURL = '/', lifeEventsList = [], ...props }: LifeEventsOverviewPageProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <Page
        headerLeftNode={
          <BreadcrumbNav>
            <BreadcrumbNavLink
              style={
                {
                  '--utrecht-icon-inset-block-start': '-0.1em',
                } as CSSProperties
              }
              current={false}
              href={`${baseURL}dashboard`}
              index={0}
            >
              <Icon>{LayoutGrid({})}</Icon>
            </BreadcrumbNavLink>
            <BreadcrumbNavSeparator>
              <Icon>{ChevronRight({})}</Icon>
            </BreadcrumbNavSeparator>
            <BreadcrumbNavLink current={true} href={`${baseURL}life-events`} index={1}>
              Levensgebeurtenissen
            </BreadcrumbNavLink>
          </BreadcrumbNav>
        }
        navigationBar={navigationBar}
        {...props}
        ref={ref}
      >
        <GridLayout templateColumns={0}>
          <GridLayoutCell columnSpan={6}>
            <Heading level={1}>Levensgebeurtenissen</Heading>
            <Paragraph>
              De overheid maakt persoonlijke overzichten voor levensgebeurtenissen zoals kind krijgen, studeren en
              overlijden. Alles wat je moet regelen met de overheid of waar je recht op hebt is te vinden in 1
              persoonlijk overzicht. Hieronder vindt u alle persoonlijke overzichten.
            </Paragraph>
            <div
              style={
                {
                  display: 'flex',
                  'flex-direction': 'column',
                  'align-items': 'start',
                  'justify-content': 'center',
                  gap: '24px',
                  paddingBlockEnd: '16px',
                } as CSSProperties
              }
            >
              {lifeEventsList.map((lifeEventListItem: LifeEventsListItem) => {
                const { name, onClick } = lifeEventListItem;
                return (
                  <MenuItem key={name} imageSrc={birthday_party} label={name} mode="horizontal" onClick={onClick} />
                );
              })}
            </div>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  },
);

LifeEventsOverviewPage.displayName = 'LifeEventsOverviewPage';
