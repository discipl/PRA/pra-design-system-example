import type { Meta, StoryObj } from '@storybook/react';
import { LifeEventsOverviewPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof LifeEventsOverviewPage> = {
  component: LifeEventsOverviewPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/LifeEventsOverviewPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof LifeEventsOverviewPage>;

export const Default: Story = {
  args: {
    lifeEventsList: [
      {
        name: 'Woning kopen: wat moet ik regelen?',
        id: 58001,
        description: 'Woning kopen: wat moet ik regelen?',
        onClick: () => {},
      },
      {
        name: '18 jaar worden: wat moet ik regelen?',
        id: 57996,
        description:
          'Als 18 jaar wordt zijn er een aantal belangrijke zaken die geregeld moeten worden. Dit overzicht helpt je hierbij.\n',
        onClick: () => {},
      },
      {
        name: 'Woning huren: wat moet ik regelen?',
        id: 58002,
        description:
          'U gaat een woning huren. Dan krijgt u onder meer te maken met het inschrijven bij een woningcorporatie, het huurcontract en mogelijk met huurtoeslag.',
        onClick: () => {},
      },
      {
        name: 'Werkloos worden: wat moet ik regelen?',
        id: 57994,
        description: 'Werkloos worden: wat moet ik regelen?',
        onClick: () => {},
      },
      {
        name: 'Met pensioen: wat moet ik regelen?',
        id: 58029,
        description: 'Met pensioen: wat moet ik regelen?',
        onClick: () => {},
      },
    ],
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-lifeeventsoverviewpage--docs#',
  },
};
