import type { Meta, StoryObj } from '@storybook/react';
import { LifeEventPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof LifeEventPage> = {
  component: LifeEventPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/LifeEventPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof LifeEventPage>;

export const Default: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-lifeeventsoverviewpage--docs#',
    lifeEvent: {
      title: '18 jaar worden: wat moet ik regelen?',
      id: 57996,
      organisation: 'Ministerie van Algemene Zaken',
      description:
        'Je bent kort geleden 18 jaar geworden. Gefeliciteerd! Nu zijn er een aantal belangrijke zaken die geregeld moeten worden. De PRA zal je hierbij helpen.\n',
      isBookmarked: false,
      content: [
        {
          ContentType: 'Plain',
          ContentId: 3,
          ContentElements: [
            <h5 className="utrecht-heading-5">
              Snel regelen
              <br />
            </h5>,
            <p className="utrecht-paragraph">
              Er zijn drie taken die snel geregeld moeten worden. Je kunt de taken toevoegen aan je takenlijst met het
              plus-teken, zodat je ze kunt afvinken als je klaar bent.
            </p>,
            <ul className="pra-unordered-list">
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                [DigiD aanvragen](regeling.digid)
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                [Zorgverzekering afsluiten](regeling.zorgverzekeringsplicht)
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                [Keuze maken over orgaandonatie](regeling.orgaandonorregistratie)
              </li>
            </ul>,
          ],
        },
        {
          ContentType: 'Plain',
          ContentId: 2,
          ContentElements: [
            <h5 className="utrecht-heading-5">Ook doen</h5>,
            <p className="utrecht-paragraph">
              De volgende zaken zijn ook belangrijk, besteed hier aandacht aan wanneer je de bovenstaande zaken geregeld
              hebt.
            </p>,
            <ul className="pra-unordered-list">
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                &nbsp; [Zorgtoeslag aanvragen](regeling.zorgtoeslag)
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                &nbsp; [Studiefinanciering aanvragen](regeling.studiefinanciering)
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                &nbsp; [Minder loonheffing vragen aan werkgever](regeling.heffingskortinguitbetaling)
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                &nbsp; [Aangifte inkomstenbelasting doen](regelen.loonbelasting)
              </li>
            </ul>,
          ],
        },
      ],
    },
  },
};
