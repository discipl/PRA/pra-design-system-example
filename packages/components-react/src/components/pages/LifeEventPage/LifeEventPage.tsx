import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import birthdayPartyImg from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode } from 'react';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { Dropdown } from '../../generic/Dropdown';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { Image } from '../../generic/Image';
import { Indicator } from '../../generic/Indicator';
import { Paragraph } from '../../generic/Paragraph';
import { Page } from '../../specific/Page';

interface LifeEventContent {
  ContentElements: (string | JSX.Element | JSX.Element[] | null)[];
  ContentType: 'Plain' | 'Expandable' | 'ExpandableSteps';
  ContentId: number;
  Label?: string;
}
interface LifeEventPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  bookmarkAction: MouseEventHandler;
  lifeEvent: {
    title: string;
    id: number;
    organisation: string;
    description: string;
    content: LifeEventContent[];
    isBookmarked: boolean;
  };
}
export interface LifeEventPageProps extends HTMLAttributes<HTMLDivElement>, LifeEventPageSpecificProps {}

export const LifeEventPage: React.ForwardRefExoticComponent<LifeEventPageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(
    (
      { baseURL = '/', lifeEvent, navigationBar, bookmarkAction, ...props }: LifeEventPageProps,
      ref: ForwardedRef<HTMLDivElement>,
    ) => {
      const { title, description, content = [], isBookmarked } = lifeEvent;
      return (
        <Page
          headerLeftNode={
            <BreadcrumbNav>
              <BreadcrumbNavLink
                style={
                  {
                    '--utrecht-icon-inset-block-start': '-0.1em',
                  } as CSSProperties
                }
                current={false}
                href={`${baseURL}}dashboard`}
                index={0}
              >
                <Icon>{LayoutGrid({})}</Icon>
              </BreadcrumbNavLink>
              <BreadcrumbNavSeparator>
                <Icon>{ChevronRight({})}</Icon>
              </BreadcrumbNavSeparator>
              <BreadcrumbNavLink current={true} href={`${baseURL}life-events`} index={1}>
                Levensgebeurtenis
              </BreadcrumbNavLink>
            </BreadcrumbNav>
          }
          navigationBar={navigationBar}
          ref={ref}
          {...props}
        >
          <GridLayout templateColumns={6}>
            <GridLayoutCell columnSpan={6}>
              <Image className="utrecht-img--scale-down" src={birthdayPartyImg} />
            </GridLayoutCell>
            <GridLayoutCell columnSpan={6}>
              <Indicator
                indicationInMins={5}
                isBookmarked={isBookmarked}
                bookmarkOnClick={(event) => bookmarkAction(event)}
              />
            </GridLayoutCell>

            <GridLayoutCell columnSpan={6}>
              <Heading level={1}> {title}</Heading>
              <Paragraph>{description}</Paragraph>
            </GridLayoutCell>
            {content.map(({ ContentElements, ContentType = 'Plain', ContentId, Label }) => {
              if (ContentType === 'Plain') {
                return (
                  <GridLayoutCell key={ContentId} columnSpan={6}>
                    {ContentElements}
                  </GridLayoutCell>
                );
              } else if (ContentType === 'Expandable') {
                return (
                  <GridLayoutCell key={ContentId} columnSpan={6}>
                    <Dropdown key={ContentId} title={Label} variant="Default">
                      {ContentElements}
                    </Dropdown>
                  </GridLayoutCell>
                );
              } else if (ContentType === 'ExpandableSteps') {
                return (
                  <GridLayoutCell key={ContentId} columnSpan={6}>
                    <Dropdown key={ContentId} title={Label} variant="Stappenplan">
                      {ContentElements}
                    </Dropdown>
                  </GridLayoutCell>
                );
              }
              return null;
            })}
          </GridLayout>
        </Page>
      );
    },
  );

LifeEventPage.displayName = 'LifeEventPage';
