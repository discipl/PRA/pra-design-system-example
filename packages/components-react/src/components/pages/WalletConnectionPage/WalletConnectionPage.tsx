import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import DownloadSVG from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Download';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import SVGGezicht from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Gezicht';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode } from 'react';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { Button } from '../../generic/Button';
import { ContentArea } from '../../generic/ContentArea';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { Paragraph } from '../../generic/Paragraph';
import { Page } from '../../specific/Page';
interface WalletConnectionPageSpecificProps {
  navigationBar?: ReactNode;
  menuTitle: string;
  walletCards: { data: { fieldName: string; value?: string }[]; dataRequestAction: MouseEventHandler; title: string }[];
}
export interface WalletConnectionPageProps extends HTMLAttributes<HTMLDivElement>, WalletConnectionPageSpecificProps {}

export const WalletConnectionPage: React.ForwardRefExoticComponent<
  WalletConnectionPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ walletCards, navigationBar, ...props }: WalletConnectionPageProps, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <Page
        alternateTheme={false}
        ref={ref}
        navigationBar={navigationBar}
        headerLeftNode={
          <BreadcrumbNav>
            <BreadcrumbNavLink
              style={
                {
                  '--utrecht-icon-inset-block-start': '-0.1em',
                } as CSSProperties
              }
              current={false}
              href={`dashboard`}
              index={0}
            >
              <Icon>{LayoutGrid({})}</Icon>
            </BreadcrumbNavLink>
            <BreadcrumbNavSeparator>
              <Icon>{ChevronRight({})}</Icon>
            </BreadcrumbNavSeparator>
            <BreadcrumbNavLink current={false} href={`search-results`} index={1}>
              Instellingen
            </BreadcrumbNavLink>
          </BreadcrumbNav>
        }
        {...props}
      >
        <GridLayout templateColumns={6}>
          <GridLayoutCell columnSpan={5} alignItems="start">
            <Heading level={1}>Wallet gegevens</Heading>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={1} alignItems="end" justifyContent="start">
            <Icon
              style={
                {
                  '--utrecht-icon-size': '55px',
                  backgroundColor: 'white',
                  borderRadius: '50%',
                  padding: '2px',
                  anchorName: '--anchor-el',
                } as CSSProperties
              }
            >
              {SVGGezicht({})}
            </Icon>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6}>
            <Paragraph style={{ fontWeight: '600' }}>
              Controleer de onderstaande gegevens die zijn opgehaald uit de EUDI-wallet:
            </Paragraph>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                gap: '20px',
                minWidth: '100%',
              }}
            >
              {walletCards.map((card) => {
                const { data, title, dataRequestAction } = card;
                return (
                  <ContentArea>
                    <b>{title}</b>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        gap: '20px',
                      }}
                    >
                      <ul onClick={dataRequestAction} style={{ listStyle: 'none' }}>
                        {data.map(({ fieldName, value }) => {
                          return (
                            <li style={{ paddingBlock: '5px' }}>
                              {fieldName}: {value}
                            </li>
                          );
                        })}
                      </ul>
                      <Button style={{ width: '50px' }} onClick={dataRequestAction} appearance="primary-action-button">
                        <Icon>{DownloadSVG({ fill: 'white' })}</Icon>
                      </Button>
                    </div>
                  </ContentArea>
                );
              })}
            </div>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  },
);

WalletConnectionPage.displayName = 'WalletConnectionPage';
