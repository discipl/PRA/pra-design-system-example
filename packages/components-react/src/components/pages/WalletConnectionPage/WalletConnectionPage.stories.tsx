import type { Meta, StoryObj } from '@storybook/react';
import { WalletConnectionPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof WalletConnectionPage> = {
  component: WalletConnectionPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/WalletConnectionPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof WalletConnectionPage>;

export const Default: Story = {
  args: {
    menuTitle: 'Wallet connectie',
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Instellingen"
        indicators={[]}
      />
    ),
    walletCards: [
      {
        title: 'Persoonsgegevens',
        data: [{ fieldName: 'voornaam' }, { fieldName: 'geboortedatum' }],
        dataRequestAction: () => {},
      },
      {
        title: 'Pensioen potjes',
        data: [{ fieldName: 'voornaam' }, { fieldName: 'geboortedatum' }],
        dataRequestAction: () => {},
      },
    ],
  },
};
