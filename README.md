<!-- @license CC0-1.0 -->

# PRA Designs System

This design system is based on the NL Design System architecture.

For more info about the NL Design System and learn about things happening in our open source community, join the `#nl-design-system` Slack via [praatmee.codefor.nl](https://praatmee.codefor.nl)!

# Table of contents

1. Persoonlijke Regelingen Assistent (PRA)
1. PRA Design System
1. Techniques
1. Build
1. Run for development
1. Running tests

## Persoonlijke Regelingen Assistent (PRA)

The Persoonlijke Regelingen Assistent (Personal Arrangements Assistant, PRA) is an ICTU project. The project aims to give citizens more insight into government regulations. So that, for example, they can receive the support they are entitled to. Or not being surprised by a tax bill that is higher than they expect. PRA falls under the Innovation in Services program of the Ministry of the Interior.

For more inormation about the PRA, please check: [PRA overview](https://gitlab.com/discipl/PRA/)

## The PRA DS

The PRA Design System is a disigns system that consists a combination of components from the NL Design System community and components that are created for the PRA specific.
This project consists of 6 packages:

1. Components-css - The styling files of the compontents
1. Components-react - The React components
1. Storybook - The PRA DS Storybook environment
1. Assets - The images and icons that can be used in the PRA
1. Design Tokens - The design tokens used in the PRA, imported from the Figma file
1. Font - The RijksOverheid Fonts

## Prerequisite

- Node.js (>=v20) and npm
- pnpm (v8)

## Build

Install the required packages for this project: `pnpm i`

To build all packages run:
`pnpm run build`

To build a specificic package use:

- `build:assets`
- `build:storybook`
- `build:components-react`
- `build:design-tokens`

## Run Storybook for development

Install the required packages for this project: `pnpm i`

Then build the packages:
`pnpm run build`

Then execute the following command to start storybook:
`npm run storybook`

## Running tests

Before running the tests first install all node-modules:
`pnpm i`

And build the packages.

To run the Storybook tests, make sure that Storybook is already running and then use the following command:
`npm run test-storybook`

Then execute the following command:
`npm run storybook`

## Release & publish a new version of the Design System

Make sure that the NPM_TOKEN environment variable is set.
As a workaround you can set it in the command line:
`NPM_TOKEN=<token>`

To release a new version of the package(s)
`pnpm run release`

To publish a new version of the package(s)
`pnpm run publish`

## Demo of the storybook components

The demo environment is automatically build with Gitlab Pages.
It can be found here: [https://discipl.gitlab.io/PRA/pra-design-system/](https://discipl.gitlab.io/PRA/pra-design-system/)

## Code of Conduct

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community. Read [our Code of Conduct](CODE_OF_CONDUCT.md) if you haven't already.

## License

This project is free and open-source software licensed under the [European Union Public License (EUPL) v1.2](LICENSE.md). Documentation is licensed as [Creative Commons Zero 1.0 Universal (`CC0-1.0`)](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

For information about proprietary assets in this repository, please carefully read the [NOTICE file](NOTICE.md).
